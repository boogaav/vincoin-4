<?php

use Illuminate\Database\Seeder;
use App\Wallet;

class WalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear table
//        \DB::table((new Wallet())->getTable())->truncate();

        Wallet::insert([
//            [ 'name' => 'mac-os', 'label' => 'macos', 'download_link' => 'storage/wallets/vincoin-gui-osx-1.6.2.zip', 'version' => '1.6.2' ],
//            [ 'name' => 'win-pc', 'label' => 'windows pc', 'download_link' => 'storage/wallets/vincoin-gui-win32-1.6.2.zip', 'version' => '1.6.2' ],
//            [ 'name' => 'github', 'label' => 'github', 'download_link' => 'storage/wallets/vincoin-1.6.2.zip', 'version' => '1.6.2' ],

            [ 'name' => 'linux', 'label' => 'linux', 'download_link' => 'storage/wallets/vincoin-cli-linux.tar-1.6.3.gz', 'version' => '1.6.3' ],
        ]);
    }
}
