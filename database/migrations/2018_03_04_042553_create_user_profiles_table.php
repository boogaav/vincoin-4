<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wallet_number')->unique()->nullable();
            $table->string('telegram_nickname')->nullable();
            $table->integer("bonus_tokens")->default("0");
//            $table->string("referrer_id")->nullable();
            $table->unsignedInteger("referred_by")->nullable();
            $table->boolean("is_telegram_joined")->default(false);
            $table->boolean("is_facebook_joined")->default(false);
            $table->boolean("is_twitter_joined")->default(false);
            $table->unsignedInteger("user_id")->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
