<?php

return [
    'default_locale' => 'en',
    'site_locales' => ['ru', 'vn']
];
