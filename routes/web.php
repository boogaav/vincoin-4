<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \App\Services\Localization\LocalizationHelper;

Route::group(['prefix' => LocalizationHelper::getRoutePrefix(), 'middleware' => ['localeChecker', 'web']], function () {
//Route::group(['middleware' => ['web']], function () {
    Route::get('/', "SiteController@index")->name("site.home");

    Route::get('/auth', 'AuthController@showAuthForm')->name('auth.showLogin');

    Route::get('/account', 'SiteController@showAccountPage')->name('site.showAccount')->middleware('auth');

    // Remove on prod
//    Route::get('/style-guide', "SiteController@styleGuide");

    Route::post('/contact-us', 'SiteController@contactUsProcess')->name('site.contactUsProcess');

    Route::post('/auth/check', 'AuthController@checkAuth')->name('auth.check');
    Route::post('/auth/login', 'AuthController@login')->name('auth.login');
    Route::post('/auth/logout', 'AuthController@logout')->name('auth.logout');

    Route::post('/account/update', 'SiteController@updateAccount')->name('site.updateAccount')->middleware('auth');

    Route::post('/wallets/download', 'SiteController@trackWallets')->name('site.trackWallets');

    Route::get('/smart-contract', 'SiteController@smartContract')->name('site.smartContract');

    // Password reset routes
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


