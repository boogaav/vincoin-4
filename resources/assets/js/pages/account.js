
import Form from 'form-backend-validation';
import FormHelper from '../utils/form-helper';

$(function() {
    const $profileForm = $(".profile-form"),
          $joinGroupBtn = $('.account-details__list .join-group'),
          $accUpdateBtn = $('.update-acc-btn');
    let requestIsPending = false;

    $profileForm.on('submit', function (event) {
        event.preventDefault();

        const self = this;
        const postUrl = $(this).attr('action');
        const $profileField = $(self).find(".form-control"),
              profileFieldVal = $profileField.val(),
              $tokensPotential = $(self).next(".tokens-potential"),
              tokensPotentialExist = $tokensPotential.length;
        const $editField = $(self).parents(".account-details__item");

        if (profileFieldVal !== '') {
            FormHelper.hideErrors($(self));
            FormHelper.processingForm($(self), true);

            const profileForm = new Form({
                [$profileField.attr('name')]: profileFieldVal,
            });

            if (! requestIsPending) {
                requestIsPending = true;
                $accUpdateBtn.attr('disabled', true);
                profileForm.post(postUrl)
                    .then(function (response) {
                        requestIsPending = false;
                        $accUpdateBtn.attr('disabled', false);
                        Ladda.stopAll();
                        FormHelper.processingForm($(self), false);
                        FormHelper.showSuccessMsg($(self), function ($form) {
                            if (tokensPotentialExist) {
                                $tokensPotential.hide();
                            }
                            setTimeout(function () {
                                if (response.tokens_added) {
                                    updateTokensEarned($(self).data("tokens"));
                                }
                                // $form.find('.form__success-msg').hide();
                                // $(self).parent().text(profileFieldVal);
                                $editField.find(".account-details__item-info").find('.account-details__text').text(profileFieldVal);
                                $editField.find(".account-details__item-info").show();
                                $editField.find(".account-details__item-input").hide();
                                FormHelper.resetForm($(self));
                                $profileField.val(profileFieldVal);
                            }, 4000);
                        });
                    })
                    .catch(function (error) {
                        requestIsPending = false;
                        $accUpdateBtn.attr('disabled', false);
                        Ladda.stopAll();
                        FormHelper.processingForm($(self), false);
                        FormHelper.showErrors($(self), profileForm);
                        // return Promise.reject(response.response);
                        // console.log(error.response.data);
                    });
            }
        }
    });

    $joinGroupBtn.on('click', function (event) {
        const self = this;
        const postUrl = $(this).data('action');
        const joinGroupRequest = new Form({
            groupType: $(this).data("group"),
        });
        const $tokensPotential = $(self).next(".tokens-potential");

        joinGroupRequest.post(postUrl)
            .then(function (response) {
                if (response.tokens_added) {
                    updateTokensEarned($(self).data("tokens"));
                }
                if ($tokensPotential.length) {
                    $tokensPotential.hide();
                }
            })
            .catch(function (error) {
                if ($tokensPotential.length) {
                    $tokensPotential.hide();
                }
            });
        // event.preventDefault();
    });

    $(".js-account-edit-btn").on('click', function (e) {
        let $editField = $(this).parents(".account-details__item");
        $editField.find(".account-details__item-info").hide();
        $editField.find(".account-details__item-input").show();
    });
});

function updateTokensEarned(amount) {
    let tokensBefore = $(".tokens-earned").find('.amount').text();
    $(".tokens-earned").removeClass('hidden').find('.amount').text(parseInt(tokensBefore) + parseInt(amount));
}
