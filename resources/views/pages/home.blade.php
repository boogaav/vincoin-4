@extends("layouts.main")
@inject("localization_helper", "\App\Services\Localization\LocalizationHelper")

@section("content")
    <header class="main-header main-header--transparent">
      <div class="main-header__container">
        <!-- Hamburger icon -->
        <div class="main-header__container__nav-trigger js-nav-trigger"><span>@lang('navigation.main.Menu')</span></div>
        <div class="nav-desktop">
          <div class="logo-wrapper">
            <a href="{{ route('site.home') }}" class="logo">
              <span class="logo__brand-img"></span>
              <span class="logo__brand-name">Vincoin Cash</span>
            </a>
          </div>
          <div class="nav-sections">
            <div class="nav-main">
              <!-- Navigation -->
              <nav role="navigation" class="nav-wrapper">
                <ul class="nav">
                  <li class="nav__item"><a class="nav__link link-anchor" href="#block-mission">@lang('navigation.main.Mission')</a></li>
                  <li class="nav__item"><a class="nav__link link-anchor" href="#section-roadmap">@lang('navigation.main.Road map')</a></li>
                  <li class="nav__item"><a class="nav__link" href="{{ asset(config('admin.white-paper_url')) }}" target="_blank">@lang('navigation.main.White Paper')</a></li>
                  {{--<li class="nav__item"><a class="nav__link link-anchor" href="#section-about-us">@lang('navigation.main.About us')</a></li>--}}
                  <li class="nav__item">
                    <a class="nav__link" href="javascript:void(0);">
                      <div class="flex items-center">
                        @lang('navigation.main.Services') <span class="nav__link__arrow"></span>
                      </div>
                    </a>
                    <div class="nav__dropdown">
                      <div class="nav__dropdown__cover"></div>
                      <ul class="nav__dropdown__list">
                        <li class="nav__dropdown__item"><a href="#section-choose" class="nav__link link-anchor">@lang('navigation.main.Trade Vincoin Cash')</a></li>
                        <li class="nav__dropdown__item"><a href="#section-choose" class="nav__link link-anchor">@lang('navigation.main.Mine Vincoin Cash')</a></li>
                        {{--<li class="nav__dropdown__item"><a href="{{ asset(config('admin.white-paper_url')) }}" class="nav__link" target="_blank">@lang('navigation.main.White Paper')</a></li>--}}
                        <li class="nav__dropdown__item"><a href="https://chain.vincoin.cash/" class="nav__link" target="_blank">@lang('navigation.main.Block explorer')</a></li>
                        <li class="nav__dropdown__item"><a href="{{ route('site.smartContract') }}" class="nav__link">@lang('Smart Contract')</a></li>
                        <li class="nav__dropdown__item"><a href="#section-faq" class="nav__link link-anchor">@lang('navigation.main.Faq')</a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="nav-secondary">
              <nav role="navigation" class="nav-wrapper">
                <ul class="nav">
                  <li class="nav__item"><a class="nav__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
                  @guest
                    <li class="nav__item"><a class="nav__link" href="{{ route("auth.showLogin") }}">@lang('navigation.main.Sign In/Sign Up')</a></li>
                  @endguest
                  @auth
                    <li class="nav__item">
                      <a class="nav__link" href="javascript:void(0);">
                        <div class="flex items-center">
                          {{ auth()->user()->email  }} <span class="nav__link__arrow"></span>
                        </div>
                      </a>
                      <div class="nav__dropdown">
                        <div class="nav__dropdown__cover"></div>
                        <ul class="nav__dropdown__list">
                          <li class="nav__dropdown__item"><a href="{{ route("site.showAccount") }}" class="nav__link">@lang('navigation.main.Account')</a></li>
                          <li class="nav__dropdown__item">
                            <a href="{{ route('auth.logout') }}" class="nav__link" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">@lang('navigation.main.Log out')</a>
                            <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                            </form>
                          </li>
                        </ul>
                      </div>
                    </li>
                  @endauth
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div class="language-wrapper">
          <div class="language-selector nav__item">
            <a class="nav__link" href="javascript:void(0);">
              <div class="flex items-center">
                {{ $localization_helper->getCurrLocale() }} <span class="nav__link__arrow"></span>
              </div>
            </a>
            <div class="nav__dropdown">
              <div class="nav__dropdown__cover"></div>
              <ul class="nav__dropdown__list js-select-language">
                <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl() }}" class="nav__link">EN</a></li>
                <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('ru') }}" class="nav__link">RU</a></li>
                <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('vn') }}" class="nav__link">VN</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="download-btn">
          <button class="btn" type="button"><a class="link-anchor uppercase" href="#section-wallet">@lang('navigation.main.Testnet is live')</a></button>
        </div>
      </div>
    </header>

    <div class="nav-mobile" id="nav-mobile">
      <div class="nav-mobile__control nav-mobile__control--back btn-mobile--icon-back js-nav-trigger"><span>@lang('navigation.main.Back')</span></div>
      @guest
      <a href="{{ route('auth.showLogin') }}" class="nav-mobile__control nav-mobile__control--user btn-mobile--icon-user"><span>@lang('navigation.main.Sign In')</span></a>
      @endguest
      @auth
      <a href="{{ route('site.showAccount') }}" class="nav-mobile__control nav-mobile__control--user btn-mobile--icon-user"><span>@lang('navigation.main.Account')</span></a>
      @endauth
      <div class="logo-wrapper flex-no-shrink">
        <div class="inline-block">
          <a href="{{ route('site.home') }}" class="logo logo--white logo--col">
            <span class="logo__brand-img"></span>
            <span class="logo__brand-name">Vincoin Cash</span>
          </a>
        </div>
      </div>
      <div class="flex-shrink overflow-scroll">
        <!-- Navigation -->
        <nav id="ml-menu" role="navigation" class="nav-wrapper ml-menu">
          <div class="menu__wrap">
            <ul data-menu="main" class="nav nav--col menu__level">
              <li class="nav__item menu__item"><a class="nav__link menu__link link-anchor" href="#block-mission">@lang('navigation.main.Mission')</a></li>
              <li class="nav__item menu__item"><a class="nav__link menu__link link-anchor" href="#section-roadmap">@lang('navigation.main.Road map')</a></li>
              <li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ asset(config('admin.white-paper_url')) }}" target="_blank">@lang('navigation.main.White Paper')</a></li>
              {{--<li class="nav__item menu__item"><a class="nav__link menu__link link-anchor" href="#section-about-us">@lang('navigation.main.About us')</a></li>--}}
              <li class="nav__item menu__item">
                <a class="nav__link menu__link" data-submenu="submenu-1" href="#">@lang('navigation.main.Services')</a>
              </li>
              <li class="nav__item menu__item"><a class="nav__link menu__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
              @guest
                <li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ route("auth.showLogin") }}">@lang('navigation.main.Sign In/Sign Up')</a></li>
              @endguest
              @auth
                <li class="nav__item menu__item">
                  <a class="nav__link menu__link" data-submenu="submenu-2" href="javascript:void(0);">{{ auth()->user()->email  }}</a>
                </li>
              @endauth
            </ul>
            <ul data-menu="submenu-1" class="nav nav--col menu__level">
              <li class="nav__item menu__item"><a class="nav__link menu__link link-anchor" href="#section-choose">@lang('navigation.main.Trade Vincoin Cash')</a></li>
              <li class="nav__item menu__item"><a class="nav__link menu__link link-anchor" href="#section-choose">@lang('navigation.main.Mine Vincoin Cash')</a></li>
              {{--<li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ asset(config('admin.white-paper_url')) }}" target="_blank">@lang('navigation.main.White Paper')</a></li>--}}
              <li class="nav__item menu__item"><a href="https://chain.vincoin.cash/" class="nav__link menu__link" target="_blank">@lang('navigation.main.Block explorer')</a></li>
              <li class="nav__item menu__item"><a href="{{ route('site.smartContract') }}" class="nav__link menu__link">@lang('Smart Contract')</a></li>
              <li class="nav__item menu__item"><a class="nav__link menu__link link-anchor" href="#section-faq">@lang('navigation.main.Faq')</a></li>
              <li class="nav__item menu__item"><a class="nav__link menu__link submenu-back uppercase" href="javascript:void(0);">
                  <span class="back-arrow"></span>
                  @lang('navigation.main.Back')
                </a></li>
            </ul>
            <ul data-menu="submenu-2" class="nav nav--col menu__level">
              <li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ route("site.showAccount") }}">@lang('navigation.main.Account')</a></li>
              <li class="nav__item menu__item">
                <a class="nav__link menu__link" href="{{ route('auth.logout') }}" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">@lang('navigation.main.Log out')</a>
                <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </li>
              <li class="nav__item menu__item"><a class="nav__link menu__link submenu-back uppercase" href="javascript:void(0);">
                  <span class="back-arrow"></span>
                  @lang('navigation.main.Back')
                </a></li>
            </ul>
          </div>
        </nav>
      </div>

      <div class="mt-2 mb-2 mx-auto"><button data-fancybox-close class="btn btn--transparent min-w-160 uppercase js-nav-trigger" type="button">@lang('navigation.main.Close Menu')</button></div>
    </div>

    {{--<div class="notification airDrop-notify">--}}
      {{--<a href="https://t.me/Vincoinbot" class="notification__wrap" target="_blank">--}}
        {{--<div class="notification__body">@lang("Join the hottest Airdrop")</div>--}}
        {{--<div class="notification__img"></div>--}}
      {{--</a>--}}
    {{--</div>--}}

    <main class="main-container" role="main">
      <div class="section-banner" id="section-banner">
        {{--<div id="particles-js--header"></div>--}}
        <div class="section-banner__container">
          <div class="social-expand-btn"></div>
          <div class="section-banner__hero">
            <div class="section-banner__col">
              <div class="logo-spin-wrapper flex-none"><div class="logo-spin"></div></div>
              <div class="section-banner__body">
                <h1 class="section-banner__body__title">
                  <span class="text-yellow flex items-center pr-1"><span class="flex-no-grow">Vincoin Cash</span> <span class="heading-middleline"></span></span>
                  @lang('The Easiest Way <br> to Enter into Crypto!')
                </h1>
                <div class="btn-group btn-wallet-group">
                  {{--<button data-fancybox href="https://www.youtube.com/watch?v=eCfUk9kvs98" class="btn btn--white btn--play mt-16 md:mt-8">@lang('Play video') <span class="play-img"></span></button>--}}
                  <button class="btn btn-download-wallet mr-6"><a href="{{ asset($wallets['win-pc']->download_link) }}" class="icon-win js-wallet-download" data-wallet-id="2"><span class="btn-download-wallet__hint">@lang("Download wallet/miner")</span><span class="btn-download-wallet__body">@lang("Windows")</span></a></button>
                  <button class="btn btn-download-wallet lg:mt-6"><a href="{{ asset($wallets['mac-os']->download_link) }}" class="icon-mac js-wallet-download" data-wallet-id="1"><span class="btn-download-wallet__hint">@lang("Download wallet/miner")</span><span class="btn-download-wallet__body">@lang("MacOS")</span></a></button>
                </div>
                <button class="btn btn-CTA hidden sm:block mt-6" type="button"><a href="{{ asset(config('admin.white-paper_url')) }}" class="" target="_blank">@lang("navigation.main.White Paper")</a></button>
                <button class="hidden btn btn--transparent btn--join-event" type="button"><a href="https://t.me/Vincoinbot" target="_blank"><span>@lang("Join the hottest Vincoin Cash Airdrop")</span></a></button>
              </div>
            </div>

            <div class="social-wrapper">
              <div class="social-links social-links--vertical">
                <div class="social-link icon-linked_In"><a class="" href="https://www.linkedin.com/company/18756174/admin/updates/" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                      <path data-name="-e-ico_linkedin" d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-5.12 30.43h-5.13V12.492h5.13V30.43zm-2.41-19.038a2.4 2.4 0 1 1 2.41-2.4 2.4 2.4 0 0 1-2.41 2.4zM33.31 30.43h-5.12V19.339c0-1.3-.37-2.208-1.97-2.208a3.075 3.075 0 0 0-3.16 2.208V30.43h-5.12V12.492h5.12v1.714a8.626 8.626 0 0 1 5.13-1.711c1.66 0 5.12 1 5.12 7V30.43z" fill-rule="evenodd"/>
                    </svg>
                  </a></div>
                <div class="social-link icon-telegram"><a class="" href="https://t.me/vincoincash" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="41" height="41">
                      <path fill-rule="evenodd" d="M20.5 40.502C9.453 40.502.497 31.547.497 20.5.497 9.453 9.453.498 20.5.498S40.502 9.453 40.502 20.5c0 11.047-8.955 20.002-20.002 20.002zm9.208-30.415l-23.979 9.25a.689.689 0 0 0 .007 1.288l5.842 2.182 2.262 7.272a.688.688 0 0 0 1.093.329l3.257-2.655a.971.971 0 0 1 1.184-.033l5.874 4.265a.689.689 0 0 0 1.079-.416l4.303-20.7a.688.688 0 0 0-.922-.782zM15.711 23.688c-.331.307-.545.72-.606 1.168l-.32 2.379c-.043.318-.489.349-.577.042l-1.234-4.338a1.15 1.15 0 0 1 .502-1.294l11.419-7.032c.205-.127.416.151.24.314l-9.424 8.761z"/>
                    </svg>
                  </a></div>
                <div class="social-link icon-facebook"><a class="" href="https://www.facebook.com/vincoincash/" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                      <path fill-rule="evenodd" d="M19.999.004C8.974.004.004 8.974.004 19.999c0 11.024 8.97 19.995 19.995 19.995s19.994-8.971 19.994-19.995C39.993 8.974 31.024.004 19.999.004zm4.972 20.699h-3.252v11.595h-4.821V20.703h-2.291v-4.098h2.291v-2.65c0-1.899.902-4.865 4.865-4.865l3.57.013v3.978h-2.592c-.423 0-1.022.211-1.022 1.117v2.408h3.673l-.421 4.097z"/>
                    </svg>
                  </a></div>
                <div class="social-link icon-youtube"><a class="" href="https://www.youtube.com/channel/UCJqRWW07kX3m-I8IkRNUa2Q" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                      <path fill-rule="evenodd" d="M20 40.001C8.954 40.001-.001 31.047-.001 20-.001 8.954 8.954-.001 20-.001 31.047-.001 40.001 8.954 40.001 20c0 11.047-8.954 20.001-20.001 20.001zM15.537 6.782l-1.031 3.964-1.073-3.964h-1.59c.319.936.65 1.876.969 2.813.485 1.407.787 2.468.924 3.193v4.087h1.511v-4.087l1.818-6.006h-1.528zm5.566 5.025c0-.805-.137-1.392-.422-1.772-.377-.515-.907-.772-1.59-.772-.679 0-1.21.257-1.587.772-.29.38-.426.967-.426 1.772v2.633c0 .799.136 1.393.426 1.769.377.513.908.77 1.587.77.683 0 1.213-.257 1.59-.77.285-.376.422-.969.422-1.769v-2.633zm5.04-2.453h-1.362v5.749c-.302.427-.589.638-.862.638-.182 0-.289-.108-.319-.319-.017-.045-.017-.211-.017-.53V9.354h-1.358v5.948c0 .53.045.89.12 1.118.137.38.44.559.879.559.497 0 1.014-.302 1.557-.924v.82h1.362V9.354zm4.629 10.812c-.273-1.186-1.244-2.062-2.412-2.192-2.768-.309-5.568-.311-8.356-.309-2.789-.002-5.59 0-8.357.309-1.168.13-2.138 1.006-2.411 2.192-.389 1.69-.394 3.535-.394 5.275 0 1.74 0 3.585.389 5.275.273 1.186 1.243 2.062 2.412 2.192 2.767.309 5.568.311 8.356.309 2.789.002 5.589 0 8.356-.309 1.168-.13 2.139-1.006 2.412-2.192.389-1.69.391-3.535.391-5.275 0-1.74.002-3.585-.386-5.275zm-1.621 9.188c-.373.541-.926.807-1.631.807s-1.243-.253-1.632-.762c-.286-.373-.433-.959-.433-1.75v-2.607c0-.795.13-1.377.416-1.754.39-.509.926-.763 1.616-.763.677 0 1.213.254 1.59.763.283.377.417.959.417 1.754v1.541h-2.697v1.32c0 .689.226 1.032.69 1.032.331 0 .525-.18.602-.54.013-.074.03-.374.03-.914h1.375v.196c0 .435-.015.735-.028.869a1.914 1.914 0 0 1-.315.808zm-2.353-3.894h1.349v-.688c0-.689-.226-1.033-.672-1.033-.451 0-.677.344-.677 1.033v.688zm-3.606 4.701c-.48 0-.943-.266-1.394-.823v.721h-1.349V20.07h1.349v3.262c.435-.536.898-.807 1.394-.807.536 0 .898.283 1.077.841.09.299.137.791.137 1.495v2.964c0 .689-.047 1.185-.137 1.5-.18.554-.541.836-1.077.836zm-.135-5.406c0-.672-.198-1.016-.587-1.016-.221 0-.447.105-.672.328v4.536c.225.226.451.332.672.332.389 0 .587-.332.587-1.004v-3.176zm-5.124 4.492c-.536.614-1.045.914-1.541.914-.434 0-.734-.176-.869-.553-.074-.227-.119-.582-.119-1.108v-5.884h1.349v5.48c0 .316 0 .48.012.523.032.21.135.316.316.316.27 0 .553-.208.852-.631v-5.688h1.35v7.443h-1.35v-.812zm-4.34.812h-1.501v-8.582h-1.585V20.07h4.701v1.407h-1.615v8.582zm5.498-14.318c-.44 0-.651-.348-.651-1.045v-3.163c0-.696.211-1.043.651-1.043.439 0 .65.347.65 1.043v3.163c0 .696-.211 1.045-.65 1.045z"/>
                    </svg>
                  </a></div>
                <div class="social-link icon-instagram"><a class="" href="https://www.instagram.com/vincoincash/" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                      <path fill-rule="evenodd" d="M19.999 39.994C8.974 39.994.004 31.023.004 19.999.004 8.974 8.974.004 19.999.004s19.995 8.97 19.995 19.995c0 11.024-8.97 19.995-19.995 19.995zM31.37 17.665v-4.642a4.4 4.4 0 0 0-4.396-4.395H13.023a4.4 4.4 0 0 0-4.395 4.395v13.953a4.4 4.4 0 0 0 4.395 4.396h13.952a4.4 4.4 0 0 0 4.395-4.396v-9.311zm-4.397 11.488H13.022a2.178 2.178 0 0 1-2.179-2.177v-9.311h3.394a6.184 6.184 0 0 0-.458 2.334 6.227 6.227 0 0 0 6.22 6.219 6.226 6.226 0 0 0 6.219-6.219c0-.825-.167-1.614-.461-2.334h3.396v9.311a2.179 2.179 0 0 1-2.18 2.177zm-2.09-17.894l3.353-.01.503-.001v3.854l-3.842.013-.014-3.856zm-4.885 12.742a4.007 4.007 0 0 1-4.003-4.003 3.987 3.987 0 0 1 .757-2.333 4 4 0 0 1 3.247-1.669c1.335 0 2.517.661 3.246 1.669.472.658.757 1.463.757 2.334a4.009 4.009 0 0 1-4.004 4.002z"/>
                    </svg>
                  </a></div>
                <div class="social-link icon-twitter"><a class="" href="https://twitter.com/Vincoin_Cash" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                      <path fill-rule="evenodd" d="M19.999.004C8.974.004.004 8.974.004 19.999c0 11.024 8.97 19.995 19.995 19.995s19.995-8.971 19.995-19.995c0-11.025-8.97-19.995-19.995-19.995zm8.92 15.418c.008.199.013.399.013.598 0 6.082-4.628 13.093-13.095 13.093a13.02 13.02 0 0 1-7.054-2.067c.36.042.727.064 1.098.064 2.157 0 4.14-.736 5.715-1.97a4.605 4.605 0 0 1-4.298-3.196 4.617 4.617 0 0 0 2.077-.078 4.605 4.605 0 0 1-3.691-4.512l.001-.058c.62.344 1.329.552 2.084.575a4.598 4.598 0 0 1-2.047-3.829c0-.845.227-1.636.623-2.316a13.063 13.063 0 0 0 9.485 4.81 4.603 4.603 0 0 1 7.842-4.198 9.195 9.195 0 0 0 2.923-1.117 4.615 4.615 0 0 1-2.024 2.546 9.148 9.148 0 0 0 2.643-.724 9.304 9.304 0 0 1-2.295 2.379z"/>
                    </svg>
                  </a></div>
                <div class="social-link icon-reddit"><a class="" href="https://www.reddit.com/user/Vincoin_cash/" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                      <path fill-rule="evenodd" d="M20 40.001C8.954 40.001 0 31.046 0 20 0 8.954 8.954-.001 20-.001S40.001 8.954 40.001 20c0 11.046-8.955 20.001-20.001 20.001zm11.134-23.433c-.828 0-1.595.334-2.182.9-2.159-1.428-5.082-2.339-8.318-2.455l1.768-5.589 4.785 1.127c-.001.024-.007.045-.007.07a2.592 2.592 0 0 0 2.59 2.589 2.593 2.593 0 0 0 2.589-2.589 2.592 2.592 0 0 0-2.589-2.589c-1.096 0-2.03.687-2.407 1.65l-5.159-1.214a.443.443 0 0 0-.525.297l-1.97 6.233c-3.385.04-6.45.956-8.704 2.426a3.162 3.162 0 0 0-2.139-.855 3.168 3.168 0 0 0-3.165 3.166c0 1.131.607 2.163 1.568 2.727a5.646 5.646 0 0 0-.101 1.017c0 4.682 5.729 8.489 12.772 8.489 7.044 0 12.774-3.807 12.774-8.489 0-.325-.034-.644-.088-.959a3.14 3.14 0 0 0 1.674-2.785 3.17 3.17 0 0 0-3.166-3.167zm1.263 5.058c-.458-1.364-1.408-2.604-2.725-3.633a2.265 2.265 0 0 1 1.461-.538 2.283 2.283 0 0 1 2.28 2.28 2.26 2.26 0 0 1-1.016 1.891zm-8.05 2.166a1.885 1.885 0 0 1 0-3.768 1.885 1.885 0 0 1 0 3.768zm.226 3.077a.443.443 0 0 1-.001.627c-.95.95-2.44 1.411-4.557 1.411-.005 0-.009-.004-.015-.004s-.01.004-.016.004c-2.116 0-3.607-.461-4.556-1.411a.444.444 0 0 1 .627-.627c.775.775 2.059 1.152 3.929 1.152.006 0 .01.003.016.003s.01-.003.015-.003c1.871 0 3.156-.377 3.93-1.152a.445.445 0 0 1 .628 0zm-8.913-3.077a1.884 1.884 0 1 1 0-3.769 1.884 1.884 0 0 1 0 3.769zM29.769 8.918a1.704 1.704 0 0 1 0 3.406 1.706 1.706 0 0 1-1.704-1.703c0-.939.765-1.703 1.704-1.703zM7.51 21.559a2.271 2.271 0 0 1-.922-1.824 2.282 2.282 0 0 1 2.278-2.28c.512 0 1.004.18 1.404.494-1.325 1.022-2.286 2.253-2.76 3.61z"/>
                    </svg>
                  </a></div>
                <div class="social-link-group icon-medium-group">
                  <div class="social-link icon-medium icon-medium__main">
                    <a class="" href="javascript:void(0);">
                      <svg class="" xmlns="http://www.w3.org/2000/svg" width="40" height="40"><path data-name="Фигура 1171" d="M20 0a20 20 0 1 0 20 20A19.994 19.994 0 0 0 20 0zm11.76 11.07l-1.83 1.81a.559.559 0 0 0-.21.53v13.18a.568.568 0 0 0 .21.53l1.83 1.8v.4h-9.23v-.4l1.86-1.8a.552.552 0 0 0 .18-.53V15.9L19.3 29.32h-.72L12.43 15.9v8.99a1.262 1.262 0 0 0 .34 1.04l2.47 2.99v.4h-7v-.4l2.46-2.99a1.192 1.192 0 0 0 .32-1.04V14.5a.906.906 0 0 0-.29-.77l-2.15-2.66v-.39h6.82l5.22 11.59 4.65-11.59h6.49v.39z" fill-rule="evenodd"/></svg>
                    </a>
                  </div>
                  <div class="links-hidden">
                    <div class="social-link icon-medium icon-medium__en">
                      <a class="" href="https://medium.com/@vincoincash_eng" target="_blank">
                        <svg class="" xmlns="http://www.w3.org/2000/svg" width="41" height="41"><path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-3.48 19.01v2.37H11.1v2.87a1.3 1.3 0 0 0 .23.86 1.032 1.032 0 0 0 .78.26h5.71l.15 2.41c-1.35.14-3.65.22-6.9.22a3.332 3.332 0 0 1-2.36-.81A2.875 2.875 0 0 1 7.8 25v-9a2.875 2.875 0 0 1 .91-2.19 3.332 3.332 0 0 1 2.36-.81c3.25 0 5.55.08 6.9.22l-.15 2.43h-5.71a1.032 1.032 0 0 0-.78.26 1.259 1.259 0 0 0-.23.84v2.26h5.92zm15.85 7.74a1.03 1.03 0 0 1-1.16 1.18h-2.04a1.207 1.207 0 0 1-.78-.22 2.232 2.232 0 0 1-.54-.75l-3.9-7.92a13.077 13.077 0 0 1-.9-2.31h-.24a17.292 17.292 0 0 1 .15 2.37v8.83h-3.12V14.25a1.04 1.04 0 0 1 1.18-1.18h2.01a1.2 1.2 0 0 1 .77.22 2.133 2.133 0 0 1 .54.75l3.77 7.69c.36.72.72 1.51 1.08 2.39h.21c-.08-1.12-.13-1.95-.13-2.5v-8.55h3.1v13.68z" fill-rule="evenodd"/></svg>
                      </a>
                    </div>
                    <div class="social-link icon-medium icon-medium__vn">
                      <a class="" href="https://medium.com/@vincoincash_vn" target="_blank">
                        {{--<svg xmlns="http://www.w3.org/2000/svg" width="41" height="41"><path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-2.1 28.18h-3.75L9.99 13.33h3.07l3.47 11.63L20 13.33h3.05zm9.11 0h-2.97V17.4h2.97v10.78zm.06-12.49a.345.345 0 0 1-.38.4H24.9a.433.433 0 0 1-.31-.11.4.4 0 0 1-.11-.29v-1.97a.371.371 0 0 1 .42-.42h2.29a.349.349 0 0 1 .28.12.436.436 0 0 1 .1.3v1.97z" fill-rule="evenodd"/></svg>--}}
                        <svg xmlns="http://www.w3.org/2000/svg" width="41" height="41">
                          <path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-5.69 27.964H11.1L6.5 13.277h3.04l3.43 11.512 3.43-11.512h3.02zm17.77 0h-2.36l-6.14-9.685v9.685h-2.95V13.277h2.36l6.15 9.686v-9.686h2.94v14.687z" fill-rule="evenodd"/>
                        </svg>
                      </a>
                    </div>
                    <div class="social-link icon-medium icon-medium__ru">
                      <a class="" href="https://medium.com/@vincoincash_ru" target="_blank">
                        <svg class="" xmlns="http://www.w3.org/2000/svg" width="41" height="41"><path d="M13.53 15.39l-2.37.03v4.17h2.37a2.625 2.625 0 0 0 1.77-.44 2.281 2.281 0 0 0 .47-1.67 2.229 2.229 0 0 0-.46-1.65 2.659 2.659 0 0 0-1.78-.44zM20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-4.32 27.93l-1.12-4.54a1.817 1.817 0 0 0-.57-.98 1.673 1.673 0 0 0-1.02-.27l-2.31-.02v5.81H7.93V13.07c1.34-.13 3.22-.2 5.67-.2a7.206 7.206 0 0 1 4.24.95c.83.63 1.25 1.78 1.25 3.45a4.35 4.35 0 0 1-.66 2.56 2.99 2.99 0 0 1-2.19 1.12v.11a2.7 2.7 0 0 1 2.12 2.24l1.29 4.63h-3.47zm17.38-5.92q0 3.405-1.3 4.8c-.87.94-2.37 1.4-4.49 1.4-2.24 0-3.82-.46-4.73-1.4s-1.37-2.53-1.37-4.8v-8.94h3.28v8.94a4.911 4.911 0 0 0 .57 2.8 3.287 3.287 0 0 0 4.18 0 4.911 4.911 0 0 0 .57-2.8v-8.94h3.29v8.94z" fill-rule="evenodd"/></svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="scrolldown-icon-wrapper">
            <img class="scrolldown-icon" src="/images/gif/animated_icon.gif" alt="">
          </div>

        </div>
      </div>

      <div class="section-intro">
        <div class="section-intro__container container">
          <div class="cards media-cards js-mobile-slider">
            <div class="card"><a data-fancybox href="{{ config("admin.project_video_links.".$localization_helper::getCurrLocale().".easy_way_try_crypto") }}">
                <img src="{{ asset("images/common/video-thumbnails/img_the_easiest_way.jpg") }}" class="media-cards__overlay">
                <div class="media-cards__inner">
                  <div class="media-cards__title">@lang("Vincoin Cash - The Easiest Way to Try Crypto!")</div>
                  <div class="media-cards__play-ico"><div class="circle-1"></div><div class="circle-2"></div></div>
                </div>
              </a></div>
            <div class="card"><a data-fancybox href="{{ config("admin.project_video_links.".$localization_helper::getCurrLocale().".how_to_mine") }}">
                <img src="{{ asset("images/common/video-thumbnails/img_how to_beome_a_miner.jpg") }}" class="media-cards__overlay">
                <div class="media-cards__inner">
                  <div class="media-cards__title">@lang("How to become a miner in 3 steps!")</div>
                  <div class="media-cards__play-ico"><div class="circle-1"></div><div class="circle-2"></div></div>
                </div>
              </a></div>
            <div class="card"><a data-fancybox href="{{ config("admin.project_video_links.".$localization_helper::getCurrLocale().".how_to_create_wallet") }}">
                <img src="{{ asset("images/common/video-thumbnails/img_how_to_register.jpg") }}" class="media-cards__overlay">
                <div class="media-cards__inner">
                  <div class="media-cards__title">@lang("How to register and create a VINCOIN wallet in 3 steps!")</div>
                  <div class="media-cards__play-ico"><div class="circle-1"></div><div class="circle-2"></div></div>
                </div>
              </a></div>
          </div>
          <div class="block-idea" id="block-idea">
            <div class="block-idea__body">
              <h2 class="heading heading--md text-underline mb-12 lg:mb-8"><span>@lang("Vincoin Cash idea")</span></h2>
              <p class="paragraph">
                @lang("Vincoin Cash tries to help everyone to understand all the nuances of the crypto industry related to the blockchain because this technology is our future. Millions of people are interested in using blockchain technology, and can see the potential for profit, however, most of them don’t know anything about the world of cryptocurrency.  Most people still perceive interaction with the cryptocurrency as something very complex, incomprehensible and not reliable. Vincoin Cash is the solution to this problem. We are on a mission to build a more open, accessible and fair financial future!")
              </p>
              <button class="btn btn--red mt-8 sm:mx-auto" type="button">
                <a class="" href="{{ asset(config('admin.white-paper_url')) }}" target="_blank">@lang('navigation.main.White Paper')</a>
              </button>
              {{--<div class="absolute pin-t pin-l z-20">--}}
                {{--<h2 class="heading heading--md text-underline mb-12 lg:mb-8"><span>@lang("Vincoin Cash idea")</span></h2>--}}
                {{--<p class="paragraph">--}}
                  {{--@lang("Vincoin Cash tries to help everyone to understand all the nuances of the crypto industry related to the blockchain because this technology is our future. Millions of people are interested in using blockchain technology, and can see the potential for profit, however, most of them don’t know anything about the world of cryptocurrency.  Most people still perceive interaction with the cryptocurrency as something very complex, incomprehensible and not reliable. Vincoin Cash is the solution to this problem. We are on a mission to build a more open, accessible and fair financial future!")--}}
                {{--</p>--}}
                {{--<button class="btn btn--red mt-8" type="button">--}}
                  {{--<a class="" href="{{ asset(config('admin.white-paper_url')) }}" target="_blank">@lang('navigation.main.White Paper')</a>--}}
                {{--</button>--}}
              {{--</div>--}}
              <img class="block-idea__cover-txt z-10 hidden" src="/images/idea/idea-txt.svg" alt="">
            </div>
            <div class="block-idea__img">
              <img class="lazy animated fadeIn" data-src="/images/idea/vnc-idea.png" width="502" height="482" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
            </div>
          </div>
          <div class="block-mission" id="block-mission">
            <div class="block-mission__img">
              <img class="lazy animated fadeIn" data-src="/images/mission/vnc_mission.png" width="414" height="517" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
            </div>
            <div class="block-mission__body">
              <h2 class="heading heading--md text-underline mb-12 lg:mb-8"><span>@lang("Vincoin Cash mission")</span></h2>
              <p class="paragraph">
                @lang("We are on a mission to build a more open, accessible, and fair financial future, one piece of software at a time. Our technology is revolutionizing the financial services industry by empowering millions across the globe to authenticate and transact immediately and without costly intermediaries.<br><br> Vincoin Cash doesn't have a person or institution who create new coins.<br> New coins are created by miners, and those miners could be you, your friends, family or colleagues.<br> Vincoin Cash can be created by your own PC, or laptop without any special hardware<br> (can be mined by CPU and GPU)")
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="section-difference" id="section-difference">
        <div class="section-difference__container">
          <div class="mx-auto container section-container">
            <div class="section-difference__title">
              <h2 class="heading heading--md text-underline"><span>@lang("Why Vincoin Cash is different ?")</span></h2>
            </div>
            <div class="section-difference__cards cards--flipped cards--red">
              <div class="card card--fungible">
                <div class="flip-wrap">
                  <div class="front">
                    <div class="card__inner">
                      <div class="card__overlay"></div>
                      <div class="card__body">
                        <svg class="card__img" xmlns="http://www.w3.org/2000/svg" width="64" height="51">
                          <path fill="#F9CD3D" fill-rule="evenodd" d="M56.533 36.124v7.438h-7.466v7.437H0V14.874h7.466V7.437h7.467V-.001H64v36.125h-7.467zM7.466 17H2.133v31.875h44.8v-5.313H7.466V17zm7.467 13.812V9.562H9.6v31.875h44.8v-5.313H14.933v-5.312zM61.866 2.124H17.067v31.875h44.799V2.124zm-48 36.126a1.064 1.064 0 0 1-1.066-1.063c0-.587.477-1.063 1.066-1.063.59 0 1.067.476 1.067 1.063s-.477 1.063-1.067 1.063zm0-6.375a1.064 1.064 0 0 1-1.066-1.063c0-.587.477-1.063 1.066-1.063.59 0 1.067.476 1.067 1.063s-.477 1.063-1.067 1.063zm0-6.375a1.064 1.064 0 0 1-1.066-1.063c0-.587.477-1.063 1.066-1.063.59 0 1.067.476 1.067 1.063s-.477 1.063-1.067 1.063zm0-6.376a1.064 1.064 0 0 1-1.066-1.063c0-.586.477-1.061 1.066-1.061.59 0 1.067.475 1.067 1.061 0 .588-.477 1.063-1.067 1.063zm0-6.375a1.064 1.064 0 0 1-1.066-1.062c0-.587.477-1.062 1.066-1.062.59 0 1.067.475 1.067 1.062 0 .587-.477 1.062-1.067 1.062zm-3.2 1.063c.589 0 1.067.476 1.067 1.062 0 .587-.478 1.063-1.067 1.063A1.064 1.064 0 0 1 9.6 14.874c0-.586.477-1.062 1.066-1.062zm0 6.375c.589 0 1.067.476 1.067 1.062 0 .587-.478 1.063-1.067 1.063A1.064 1.064 0 0 1 9.6 21.249c0-.586.477-1.062 1.066-1.062zm0 6.375c.589 0 1.067.475 1.067 1.062 0 .587-.478 1.063-1.067 1.063a1.064 1.064 0 0 1 0-2.125zm0 6.375c.589 0 1.067.475 1.067 1.062 0 .588-.478 1.063-1.067 1.063a1.064 1.064 0 1 1 0-2.125zm0 6.374c.589 0 1.067.476 1.067 1.063s-.478 1.063-1.067 1.063A1.064 1.064 0 0 1 9.6 40.374c0-.587.477-1.063 1.066-1.063zM16 40.374c0-.587.477-1.063 1.067-1.063.588 0 1.066.476 1.066 1.063s-.478 1.063-1.066 1.063c-.59 0-1.067-.476-1.067-1.063zm6.4 0c0-.587.477-1.063 1.066-1.063.59 0 1.067.476 1.067 1.063s-.477 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063zm6.4 0c0-.587.477-1.063 1.066-1.063.59 0 1.067.476 1.067 1.063s-.477 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063zm6.4 0c0-.587.477-1.063 1.066-1.063.589 0 1.067.476 1.067 1.063s-.478 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063zm6.4 0c0-.587.477-1.063 1.066-1.063.589 0 1.067.476 1.067 1.063s-.478 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063zm6.4 0c0-.587.477-1.063 1.067-1.063.588 0 1.066.476 1.066 1.063s-.478 1.063-1.066 1.063c-.59 0-1.067-.476-1.067-1.063zm5.333-3.187c0 .587-.478 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063c0-.587.477-1.063 1.066-1.063.589 0 1.067.476 1.067 1.063zm-6.4 0c0 .587-.477 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063c0-.587.477-1.063 1.066-1.063.59 0 1.067.476 1.067 1.063zm-6.4 0c0 .587-.477 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063c0-.587.477-1.063 1.066-1.063.59 0 1.067.476 1.067 1.063zm-6.4 0c0 .587-.478 1.063-1.066 1.063-.59 0-1.067-.476-1.067-1.063s.477-1.063 1.067-1.063c.588 0 1.066.476 1.066 1.063zm-6.4 0c0 .587-.478 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063c0-.587.477-1.063 1.066-1.063.589 0 1.067.476 1.067 1.063zm-6.4 0c0 .587-.478 1.063-1.067 1.063a1.064 1.064 0 0 1-1.066-1.063c0-.587.477-1.063 1.066-1.063.589 0 1.067.476 1.067 1.063z"/>
                        </svg>
                        <div class="card__title">@lang("Vincoin Cash is fungible")</div>
                      </div>
                    </div>
                  </div>
                  <div class="back">
                    <div class="card__inner">
                      <div class="card__body">
                        <div class="card__desc">@lang("Vincoin Cash is fungible because it is private by default. Units of Vincoin Cash cannot be blacklisted by vendors or exchanges due to their association in previous transactions.")</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card card--unlinkable">
                <div class="flip-wrap">
                  <div class="front">
                    <div class="card__inner">
                      <div class="card__overlay"></div>
                      <div class="card__body">
                        <svg class="card__img" xmlns="http://www.w3.org/2000/svg" style="" width="58" height="52">
                          <path fill="#F9CD3D" fill-rule="evenodd" d="M0 52V-.001h20.797l.288.41.001.001 4.554 6.457H58V52H0zM56.066 8.83h-29.04l3.46 4.905h25.58V8.83zm0 6.868H29.967v-.031a.966.966 0 0 1-.787-.41L24.647 8.83h-.012l-4.833-6.868H1.933v48.076h54.133v-34.34zM23.2 25.509c0-3.247 2.601-5.886 5.8-5.886 3.198 0 5.8 2.639 5.8 5.886v3.925h3.866v15.698H19.333V29.434H23.2v-3.925zm9.666 0c0-2.164-1.734-3.924-3.866-3.924-2.133 0-3.867 1.76-3.867 3.924v3.925h7.733v-3.925zm-11.6 5.887V43.17h15.467V31.396H21.266zm7.734.981c2.132 0 3.866 1.76 3.866 3.925a3.909 3.909 0 0 1-2.899 3.785v1.12a.973.973 0 0 1-.967.981.973.973 0 0 1-.967-.981v-1.12a3.91 3.91 0 0 1-2.9-3.785c0-2.165 1.734-3.925 3.867-3.925zm0 5.887c1.066 0 1.933-.88 1.933-1.962 0-1.083-.867-1.963-1.933-1.963s-1.934.88-1.934 1.963c0 1.082.868 1.962 1.934 1.962z"/>
                        </svg>
                        <div class="card__title">@lang("Unlinkable")</div>
                      </div>
                    </div>
                  </div>
                  <div class="back">
                    <div class="card__inner">
                      <div class="card__body">
                        <div class="card__desc">@lang("Being a great feature, untreaceability doesn’t protect a receiver from defining his or her balance through inspecting incoming messages to the user’s public address.")</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card card--resistant">
                <div class="flip-wrap">
                  <div class="front">
                    <div class="card__inner">
                      <div class="card__overlay"></div>
                      <div class="card__body">
                        <svg class="card__img" xmlns="http://www.w3.org/2000/svg" width="64" height="52">
                          <path fill="#F9CD3D" fill-rule="evenodd" d="M62.852 52H35.874a1.128 1.128 0 0 1-1.148-1.135V1.135c0-.639.503-1.135 1.148-1.135h26.978c.573 0 1.076.496 1.147 1.064v49.801c0 .638-.502 1.135-1.147 1.135zM61.775 2.199H37.022v4.398h24.753V2.199zm0 6.597H37.022v33.059h24.753V8.796zm0 35.258H37.022v5.675h24.753v-5.675zM49.363 49.02c-1.292 0-2.368-1.064-2.368-2.341 0-1.277 1.076-2.341 2.368-2.341 1.291 0 2.368 1.064 2.368 2.341 0 1.277-1.077 2.341-2.368 2.341zm9.112-32.278h-14.78l2.726 2.128c.502.355.574 1.064.216 1.561a1.031 1.031 0 0 1-.862.426c-.287 0-.502-.071-.717-.214l-5.31-4.114c-.071 0-.071-.071-.071-.071a43.381 43.381 0 0 1-.215-.213c0-.071-.072-.071-.072-.142 0 0-.072 0-.072-.07v-.284-.143-.354c0-.142.072-.213.144-.355 0-.071.071-.071.071-.071l.144-.141 5.309-4.115c.502-.355 1.22-.284 1.579.212.358.497.287 1.207-.216 1.561l-2.726 2.129h14.852c.646 0 1.148.497 1.148 1.134 0 .639-.502 1.136-1.148 1.136zM46.637 33.768c.215 0 .574 0 .86.355.646.709 1.507 1.064 2.583 1.135h.359c.574 0 1.076-.142 1.435-.355s.503-.497.503-.993c0-.355-.144-.639-.503-.851-.359-.284-.861-.497-1.435-.71-.574-.213-1.219-.426-1.865-.71a4.935 4.935 0 0 1-1.722-1.276c-.503-.568-.718-1.348-.718-2.2 0-1.135.359-2.057 1.148-2.625.503-.425 1.148-.638 1.866-.78v-1.489c0-.639.502-1.135 1.148-1.135.645 0 1.148.496 1.148 1.135v1.489c1.147.213 2.08.78 2.726 1.703.144.213.215.425.215.709s-.071.568-.287.781a1.023 1.023 0 0 1-1.578 0c-.646-.639-1.363-.923-2.296-.923-1.722 0-1.722.781-1.722 1.135 0 .355.143.639.574.923.359.284.861.496 1.435.709.646.213 1.22.426 1.866.709.645.284 1.219.71 1.578 1.207.43.567.717 1.276.717 2.128 0 1.205-.43 2.199-1.291 2.837-.502.426-1.22.71-2.009.852v1.489c0 .639-.502 1.135-1.148 1.135a1.126 1.126 0 0 1-1.148-1.135v-1.489c-.502-.072-1.076-.284-1.507-.497-.789-.426-1.435-.922-1.794-1.419a1.36 1.36 0 0 1-.215-.709c0-.284.072-.568.287-.78.216-.213.502-.355.79-.355zM28.125 52H1.148A1.127 1.127 0 0 1 0 50.865V1.135C0 .496.502 0 1.148 0h26.977c.646 0 1.148.496 1.148 1.064v49.801c0 .638-.502 1.135-1.148 1.135zM26.977 2.199H2.296v4.398h24.681V2.199zm0 6.597H2.296v33.059h24.681V8.796zm0 35.258H2.296v5.675h24.681v-5.675zm-12.34.284c1.291 0 2.367 1.064 2.367 2.341 0 1.277-1.076 2.341-2.367 2.341-1.292 0-2.368-1.064-2.368-2.341 0-1.277 1.076-2.341 2.368-2.341zM5.668 33.98h14.709l-2.727-2.128c-.502-.354-.574-1.064-.215-1.561.359-.496 1.076-.567 1.578-.212l5.31 4.115c.072 0 .072.07.072.07l.215.213c0 .071.071.071.071.142 0 0 .072 0 .072.071V35.328c0 .143-.072.213-.143.355 0 .071-.072.071-.072.071l-.143.142-5.31 4.115a1.401 1.401 0 0 1-.717.212c-.288 0-.646-.141-.862-.425-.358-.497-.286-1.206.216-1.561l2.726-2.128H5.668c-.646 0-1.148-.497-1.148-.993 0-.639.502-1.136 1.148-1.136zm11.408-6.384v1.135c0 .639-.502 1.135-1.148 1.135a1.126 1.126 0 0 1-1.148-1.135v-1.064h-1.435v1.064c0 .639-.502 1.135-1.148 1.135a1.127 1.127 0 0 1-1.148-1.135v-1.064H9.542a1.127 1.127 0 0 1-1.147-1.136c0-.637.502-1.134 1.147-1.134v.213h1.507v-4.115c-.574 0-1.076-.497-1.076-1.064 0-.568.502-1.065 1.076-1.065v-4.114H9.542a1.127 1.127 0 0 1-1.147-1.135c0-.639.502-1.135 1.147-1.135h1.507v-1.135c0-.638.503-1.135 1.148-1.135.646 0 1.148.497 1.148 1.135v1.135h1.435v-1.135c0-.638.502-1.135 1.148-1.135.646 0 1.148.497 1.148 1.135v1.135c2.153.212 3.874 1.987 3.874 4.185 0 1.206-.573 2.342-1.435 3.122.862.78 1.435 1.845 1.435 3.121 0 2.2-1.721 3.973-3.874 4.186zm-.574-12.557h-3.229v4.115h3.229c1.148 0 2.081-.923 2.081-2.057 0-1.136-.933-2.058-2.081-2.058zm0 6.314h-3.229v4.114h3.229c1.148 0 2.081-.921 2.081-2.057 0-1.135-.933-2.057-2.081-2.057z"/>
                        </svg>
                        <div class="card__title">@lang("Analysis resistant")</div>
                      </div>
                    </div>
                  </div>
                  <div class="back">
                    <div class="card__inner">
                      <div class="card__body">
                        <div class="card__desc">@lang("Vincoin Cash blockchain analysis resistance results from its unlinkability, and is rarely analyzed by confused persons within the blockchain <a href='https://chain.vincoin.cash/' class='nav__link' target='_blank'>chain.vincoin.cash</a>.")</div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card card--untraceable">
                <div class="flip-wrap">
                  <div class="front">
                    <div class="card__inner">
                      <div class="card__overlay"></div>
                      <div class="card__body">
                        <svg class="card__img" xmlns="http://www.w3.org/2000/svg" width="50" height="52">
                          <path fill="#F9CD3D" fill-rule="evenodd" d="M45.899 28.174c-4.026 9.829-10.789 17.768-20.101 23.597a1.51 1.51 0 0 1-1.596 0C14.89 45.942 8.127 38.002 4.101 28.174.74 19.969.016 12.171.001 7.075a1.518 1.518 0 0 1 .442-1.076 1.505 1.505 0 0 1 1.067-.444h.015a3.962 3.962 0 0 0 2.824-1.174 4.027 4.027 0 0 0 1.184-2.866c0-.402.159-.787.442-1.071A1.505 1.505 0 0 1 7.042 0h35.916c.401 0 .784.16 1.067.444.283.284.442.669.442 1.071 0 1.084.42 2.102 1.184 2.866a3.963 3.963 0 0 0 2.824 1.175h.015c.401 0 .784.159 1.067.443.284.285.443.672.442 1.076-.015 5.096-.739 12.894-4.1 21.099zm-2.38-21.649a7.028 7.028 0 0 1-1.909-3.496H8.39a7.036 7.036 0 0 1-1.909 3.496 6.967 6.967 0 0 1-3.442 1.896C3.326 18.951 6.775 36.838 25 48.691c18.225-11.853 21.674-29.74 21.961-40.27a6.967 6.967 0 0 1-3.442-1.896zM28.691 30.343l-1.475-2.447h-4.432l-1.475 2.447a1.507 1.507 0 0 1-1.291.731h-5.857a1.51 1.51 0 0 1-1.322-.784l-1.741-3.179a1.517 1.517 0 0 1 .594-2.057 1.506 1.506 0 0 1 2.049.597l1.312 2.394h4.114l1.475-2.448a1.51 1.51 0 0 1 1.291-.73h6.134c.528 0 1.018.277 1.291.73l1.475 2.448h4.114l1.312-2.394c.401-.732 1.319-1 2.049-.597.73.404.996 1.324.595 2.057l-1.742 3.179a1.51 1.51 0 0 1-1.322.784h-5.857a1.508 1.508 0 0 1-1.291-.731zm8.024-16.442c.556 0 1.006.453 1.006 1.01 0 .558-.45 1.01-1.006 1.01h-3.988a1.008 1.008 0 0 1-1.006-1.01c0-.557.451-1.01 1.006-1.01h3.988zm-2.117-2.288a6.087 6.087 0 0 0-5.256 3.046 1.509 1.509 0 0 1-2.062.552 1.518 1.518 0 0 1-.55-2.069 9.114 9.114 0 0 1 7.868-4.558 9.111 9.111 0 0 1 7.866 4.556c.418.724.172 1.65-.549 2.069a1.505 1.505 0 0 1-2.062-.552 6.09 6.09 0 0 0-5.255-3.044zM17.52 13.901c.556 0 1.006.453 1.006 1.01 0 .558-.45 1.01-1.006 1.01h-3.987a1.008 1.008 0 0 1-1.006-1.01c0-.557.45-1.01 1.006-1.01h3.987zm5.2 1.309a1.506 1.506 0 0 1-2.062-.552 6.087 6.087 0 0 0-5.255-3.045 6.087 6.087 0 0 0-5.256 3.046 1.507 1.507 0 0 1-2.061.553 1.518 1.518 0 0 1-.551-2.07 9.113 9.113 0 0 1 7.868-4.557 9.113 9.113 0 0 1 7.867 4.555c.417.724.171 1.65-.55 2.07zm.732 16.512a1.008 1.008 0 0 1-1.006-1.01c0-.558.45-1.01 1.006-1.01h3.096c.556 0 1.006.452 1.006 1.01s-.45 1.01-1.006 1.01h-3.096zM25 43.694l-2.929-10.17h5.858L25 43.694z"/>
                        </svg>
                        <div class="card__title">@lang("Vincoin Cash is untraceable")</div>
                      </div>
                    </div>
                  </div>
                  <div class="back">
                    <div class="card__inner">
                      <div class="card__body">
                        <div class="card__desc">@lang("Sending and receiving addresses as well as transacted amounts are obfuscated by default. Transactions on the Vincoin Cash blockchain cannot be linked to a particular user or real-world identity.")</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card card--increase">
                <div class="flip-wrap">
                  <div class="front">
                    <div class="card__inner">
                      <div class="card__overlay"></div>
                      <div class="card__body">
                        <svg class="card__img" xmlns="http://www.w3.org/2000/svg" width="62" height="53">
                          <path fill="#F9CD3D" fill-rule="evenodd" d="M61.06 53H.935A.932.932 0 0 1 0 52.047c0-.536.409-.953.935-.953h4.387V37.219c0-.536.41-.953.936-.953h8.656c.526 0 .936.417.936.953v13.875h3.158V31.621c0-.536.41-.953.936-.953h8.597c.527 0 .936.417.936.953v19.473h3.158V33.05c0-.536.41-.952.936-.952h8.656c.527 0 .936.416.936.952v18.044h3.159V23.522c0-.536.409-.952.935-.952h8.598c.526 0 .936.416.936.952v27.572h4.328c.527 0 .936.417.877.953a.932.932 0 0 1-.936.953zM13.92 38.171H7.194v12.923h6.726V38.171zm13.685-5.597H20.88v18.52h6.725v-18.52zm13.628 1.429h-6.726v17.091h6.726V34.003zm13.686-9.528h-6.726v26.619h6.726V24.475zm-3.392-7.146c-4.679 0-8.481-3.811-8.481-8.635S46.789 0 51.527 0c4.738 0 8.539 3.811 8.539 8.635 0 4.823-3.86 8.694-8.539 8.694zm0-15.423c-3.626 0-6.668 2.977-6.668 6.729 0 3.751 2.983 6.788 6.668 6.788 3.684 0 6.668-2.977 6.668-6.729 0-3.752-2.984-6.788-6.668-6.788zm2.515 9.706c-.117.178-.175.357-.293.476-.116.179-.233.298-.35.417-.176.12-.293.239-.468.298a2.046 2.046 0 0 1-.527.238h-.058v1.251h-1.404V13.1h-.117c-.175 0-.292 0-.468-.059-.175-.059-.351-.059-.468-.119-.116 0-.292-.059-.409-.119-.175-.059-.293-.119-.526-.059l-.176-.06v-.179l.117-1.071.059-.417.351.178c.058 0 .117.06.175.06.058 0 .117.059.175.059.118.06.234.06.351.12.117.059.234.119.351.119.059 0 .176.059.293.059h.76c.117 0 .234-.059.293-.059.058-.06.175-.06.233-.119.118-.06.176-.12.234-.179l.176-.179a.458.458 0 0 0 .117-.238v-.298c0-.119-.059-.238-.059-.297-.058-.119-.058-.179-.117-.239l-.175-.178c-.058 0-.117-.06-.176-.12-.058-.059-.175-.059-.233-.119a.447.447 0 0 0-.235-.119A4.057 4.057 0 0 0 51 9.289c-.116-.059-.292-.178-.467-.238-.117-.06-.293-.119-.41-.238-.175-.059-.292-.178-.409-.297-.117-.12-.176-.239-.292-.358-.059-.119-.176-.298-.235-.417-.117-.179-.117-.297-.175-.476-.058-.179-.058-.298-.058-.477 0-.178 0-.417.058-.595l.175-.536c.059-.179.118-.357.235-.477.116-.178.233-.297.351-.417.116-.118.233-.237.409-.297a2.04 2.04 0 0 1 .526-.238h.059V3.037h1.403v1.131h.059c.117 0 .175.06.292.06.117 0 .234.059.293.059.117 0 .175.06.234.06.058 0 .175.06.233.06l.235.059v.238l-.117.953-.059.358-.292-.12c-.059 0-.117-.059-.234-.059-.059 0-.176-.06-.234-.06-.059 0-.176-.059-.293-.059-.117-.06-.175-.06-.292-.06h-.351c-.117 0-.176 0-.292.06a.443.443 0 0 0-.293.119l-.175.179c-.059.059-.059.178-.117.237 0 .06-.059.179-.059.238v.298c0 .06 0 .179.059.239 0 .119.058.178.117.238l.175.179c.059.059.176.059.234.118.059.06.175.06.234.119.059.06.176.12.234.12.117.059.293.119.468.178.175.06.292.179.468.239.117.059.292.119.409.238.175.06.293.179.41.298.117.119.175.237.292.357.117.119.176.298.234.417.117.178.117.298.175.476.059.179.059.298.059.476 0 .239 0 .417-.059.596l-.175.536zm-6.609 6.849c.292.416.234 1.071-.176 1.369l-9.884 7.98c-.234.179-.643.238-.936.119l-12.282-5.181-12.692 9.23a.925.925 0 0 1-.526.179c-.293 0-.585-.119-.761-.417-.292-.417-.175-1.012.234-1.31l13.101-9.528a.92.92 0 0 1 .878-.119l12.224 5.181 9.475-7.682c.409-.298 1.053-.238 1.345.179z"/>
                        </svg>
                        <div class="card__title">@lang("Vincoin Cash increases your capital")</div>
                      </div>
                    </div>
                  </div>
                  <div class="back">
                    <div class="card__inner">
                      <div class="card__body">
                        <div class="card__desc">@lang("Wallet encryption allows you to secure your wallet, so that a password is required before you can spend your coins.")</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card card--anonymous">
                <div class="flip-wrap">
                  <div class="front">
                    <div class="card__inner">
                      <div class="card__overlay"></div>
                      <div class="card__body">
                        <svg class="card__img" xmlns="http://www.w3.org/2000/svg" width="53" height="53">
                          <path fill="#F9CD3D" fill-rule="evenodd" d="M48 30.149a5.274 5.274 0 0 0 1.466-3.65 5.307 5.307 0 0 0-4.14-5.168l3.928-12.566C51.369 8.439 53 6.622 53 4.417A4.421 4.421 0 0 0 48.583 0a4.421 4.421 0 0 0-4.081 2.737L31.448 5.185A5.302 5.302 0 0 0 26.5 1.766a5.305 5.305 0 0 0-4.964 3.459L8.753 3.627C8.379 1.569 6.581 0 4.417 0A4.421 4.421 0 0 0 0 4.417a4.425 4.425 0 0 0 3.533 4.327v10.778A4.425 4.425 0 0 0 0 23.85c0 2.255 1.707 4.102 3.895 4.363l3.476 5.563a5.306 5.306 0 0 0-3.838 5.09c0 1.415.56 2.699 1.466 3.65C2.155 43.07 0 45.579 0 48.583v3.533c0 .488.394.884.883.884h15.9a.884.884 0 0 0 .884-.884v-2.65h22.172A4.425 4.425 0 0 0 44.167 53a4.421 4.421 0 0 0 4.416-4.417 4.425 4.425 0 0 0-3.533-4.327v-3.623h7.067a.883.883 0 0 0 .883-.884v-3.532c0-3.005-2.156-5.513-5-6.068zm-.3-3.65a3.537 3.537 0 0 1-3.533 3.534 3.538 3.538 0 0 1-3.534-3.534 3.536 3.536 0 0 1 3.534-3.532 3.536 3.536 0 0 1 3.533 3.532zm.883-24.733a2.653 2.653 0 0 1 2.65 2.651 2.653 2.653 0 0 1-2.65 2.649 2.653 2.653 0 0 1-2.65-2.649 2.653 2.653 0 0 1 2.65-2.651zM31.8 7.066c0-.05-.007-.099-.008-.148l12.392-2.324a4.402 4.402 0 0 0 3.251 4.068l-3.933 12.584a5.295 5.295 0 0 0-3.85 2.49l-4.333-7.221a6.19 6.19 0 0 0-4.987-5.8A5.265 5.265 0 0 0 31.8 7.066zm-5.184 18.676l.651-4.543h7.183a.883.883 0 0 0 .883-.883v-.344l3.567 5.945a5.27 5.27 0 0 0 1.433 4.232 6.201 6.201 0 0 0-4.75 4.341l-5.595-4.896a4.405 4.405 0 0 0-3.372-3.852zm1.65 4.291a2.653 2.653 0 0 1-2.649 2.65 2.654 2.654 0 0 1-2.651-2.65c0-1.461 1.19-2.65 2.651-2.65a2.653 2.653 0 0 1 2.649 2.65zM26.5 3.533a3.537 3.537 0 0 1 3.533 3.533A3.537 3.537 0 0 1 26.5 10.6a3.538 3.538 0 0 1-3.534-3.534A3.537 3.537 0 0 1 26.5 3.533zm-2.65 8.833h5.3a4.422 4.422 0 0 1 4.416 4.417v2.65H19.433v-2.65a4.422 4.422 0 0 1 4.417-4.417zM8.713 5.402l12.492 1.562c-.001.034-.005.068-.005.102 0 1.415.56 2.699 1.466 3.65a6.191 6.191 0 0 0-4.975 5.578L8.563 22.38A4.419 4.419 0 0 0 5.3 19.522V8.744a4.426 4.426 0 0 0 3.413-3.342zm-6.947-.985a2.654 2.654 0 0 1 2.651-2.651 2.653 2.653 0 0 1 2.649 2.651 2.652 2.652 0 0 1-2.649 2.649 2.654 2.654 0 0 1-2.651-2.649zm0 19.433a2.654 2.654 0 0 1 2.651-2.651 2.653 2.653 0 0 1 2.649 2.651 2.652 2.652 0 0 1-2.649 2.649 2.653 2.653 0 0 1-2.651-2.649zm4.082 4.158a4.4 4.4 0 0 0 2.934-3.651l8.885-5.924v1.883c0 .488.394.883.883.883h6.931l-.642 4.496a4.404 4.404 0 0 0-3.595 3.905l-7.673 6.905a5.304 5.304 0 0 0-4.233-2.913l-3.49-5.584zm2.985 7.325a3.536 3.536 0 0 1 3.533 3.533A3.537 3.537 0 0 1 8.833 42.4 3.537 3.537 0 0 1 5.3 38.866a3.536 3.536 0 0 1 3.533-3.533zm7.067 15.9H1.766v-2.65a4.423 4.423 0 0 1 4.417-4.417h5.3a4.423 4.423 0 0 1 4.417 4.417v2.65zm1.578-4.139a6.197 6.197 0 0 0-4.812-4.578 5.273 5.273 0 0 0 1.467-3.65c0-.157-.01-.313-.024-.469l7.426-6.684a4.421 4.421 0 0 0 4.082 2.737 4.417 4.417 0 0 0 4.086-2.759l5.044 4.413-17.269 10.99zm22.361.606H19.817l15.516-9.874v1.923c0 .488.395.884.883.884h7.067v3.623a4.428 4.428 0 0 0-3.444 3.444zm6.977.883a2.652 2.652 0 0 1-2.649 2.65 2.653 2.653 0 0 1-2.651-2.65 2.653 2.653 0 0 1 2.651-2.65 2.652 2.652 0 0 1 2.649 2.65zm4.417-9.717H37.099v-2.649a4.422 4.422 0 0 1 4.417-4.417h5.3a4.421 4.421 0 0 1 4.417 4.417v2.649z"/>
                        </svg>
                        <div class="card__title">@lang("No intermediaries/regulators")</div>
                      </div>
                    </div>
                  </div>
                  <div class="back">
                    <div class="card__inner">
                      <div class="card__body">
                        <div class="card__desc">@lang("Vincoin Cash blockchain transactions are not affiliated to one person, and do not need KYC. Sending and receiving addresses are automatically hidden, which guarantee’s complete security.")</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="ring-signature">
            <div class="mx-auto container section-container py-30 lg:py-15 text-center">
              <h2 class="heading heading--md text-underline mb-10"><span>@lang("Ring signatures: Untraceable payments")</span></h2>
              <p class="paragraph mb-15">@lang("We solve this dilemma by automatic creation of multiple unique one-time keys, derived from the single public key, for each p2p payment. The solution lies in a clever modification of the Diffie-Hellman exchange protocol[1]. Originally it allows two parties to produce a common secret key derived from their public keys. In our version the sender uses the receiver's public address and his own random data to compute a one-time key for the payment.")</p>
              <div class="cd-grid cd-grid-gap--lg">
                <div class="col--6 col--lg-12">
                  <div class="text-xl text-center mb-8">Current signatures solution</div>
                  <img class="lazy animated fadeIn" data-src="/images/difference/ordinary-signature.png" width="546" height="233" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
                </div>
                <div class="col--6 col--lg-12">
                  <div class="text-xl text-center mb-8">Upcoming signatures solution</div>
                  <img class="lazy animated fadeIn" data-src="/images/difference/ring-signature.png" width="545" height="230" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
                </div>
              </div>
            </div>
          </div>

          <div class="untraceable-transactions bg-grey-lighter">
            <div class="mx-auto container section-container py-30 lg:py-15 text-center">
              <h2 class="heading heading--md text-underline mb-10"><span>@lang("Untraceable transactions")</span></h2>
              <p class="paragraph mb-15">@lang("We solve this dilemma by automatic creation of multiple unique one-time keys, derived from the single public key, for each p2p payment. The solution lies in a clever modification of the Diffie-Hellman exchange protocol[1]. Originally it allows two parties to produce a common secret key derived from their public keys. In our version the sender uses the receiver's public address and his own random data to compute a one-time key for the payment.")</p>
              <div class="cd-grid cd-grid-gap--lg">
                <div class="col--4 col--lg-12 cd-flex justify-center items-end">
                  <img class="lazy animated fadeIn" data-src="/images/difference/linkable-transactions.png" width="454" height="271" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
                </div>
                <div class="col--4 col--lg-12 cd-flex justify-center items-end">
                  <img class="lazy animated fadeIn" data-src="/images/difference/untreaceable-transactions.png" width="539" height="313" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
                </div>
                <div class="col--4 col--lg-12 cd-flex justify-center items-end">
                  <img class="lazy animated fadeIn" data-src="/images/difference/unlinkable-transactions.png" width="606" height="302" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
                </div>
              </div>
            </div>
          </div>

          <div class="double-spending">
            <div class="mx-auto container section-container py-30 lg:py-15 text-center">
              <h2 class="heading heading--md text-underline mb-10"><span>@lang("Double-spending proof")</span></h2>
              <div class="cd-grid cd-grid-gap--lg">
                <div class="col--6 col--lg-12 cd-flex flex-col justify-between items-center">
                  <p class="paragraph mb-28 lg:mb-15">@lang("Fully anonymous signatures would allow spending the same funds many times which, of course, is incompatible with any payment system's principles. The problem can be fixed as follows.")</p>
                  <img class="lazy animated fadeIn my-auto" data-src="/images/difference/double-spending-1.png" width="378" height="197" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
                  <div class="text-xl font-bold mt-12">@lang("Key image via one-way function")</div>
                </div>
                <div class="col--6 col--lg-12 cd-flex flex-col justify-between items-center">
                  <p class="paragraph mb-8 lg:mb-15">@lang("All users keep the list of the used key images (compared with the history of all valid transactions it requires an insignificant amount of storage) and immediately reject any new ring signature with a duplicate key image. It will not identify the misbehaving user, but it does prevent any double-spending attempts, caused by malicious intentions or software errors.")</p>
                  <img class="lazy animated fadeIn my-auto" data-src="/images/difference/double-spending-2.png" width="588" height="168" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">
                  <div class="text-xl font-bold mt-12">@lang("Double-spending check")</div>
                </div>
              </div>
            </div>
          </div>

          {{--<div class="mirror-block">--}}
            {{--<div class="mirror-block__container container">--}}
              {{--<h2 class="mirror-block__title-mobile"><span>@lang("Vincoin Cash is secure")</span></h2>--}}
              {{--<div class="mirror-block__col mirror-block__body">--}}
                {{--<h2 class="mirror-block__body__title"><span>@lang("Vincoin Cash is secure")</span></h2>--}}
                {{--<p class="paragraph">@lang("Vincoin Cash is a decentralized cryptocurrency meaning it is secure digital cash operated by a network of users. Transactions are confirmed by distributed consensus and then immutably recorded on the blockchain. Third-parties do not need to be trusted to keep your Vincoin Cash safe.")</p>--}}
              {{--</div>--}}
              {{--<div class="mirror-block__col mirror-block__img">--}}
                {{--<img class="lazy animated fadeIn" data-src="/images/difference/img-safe.png" width="382" height="377" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<div class="mirror-block mirror-block--inverse">--}}
            {{--<div class="mirror-block__container container">--}}
              {{--<h2 class="mirror-block__title-mobile"><span>@lang("Wallet Encryption")</span></h2>--}}
              {{--<div class="mirror-block__col mirror-block__img">--}}
                {{--<img class="lazy animated fadeIn" data-src="/images/difference/img-wallet.png" width="381" height="362" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">--}}
              {{--</div>--}}
              {{--<div class="mirror-block__col mirror-block__body">--}}
                {{--<h2 class="mirror-block__body__title"><span>@lang("Wallet Encryption")</span></h2>--}}
                {{--<p class="paragraph">@lang("Wallet encryption allows you to secure your wallet, so that you can view transactions and your account balance, but are required to enter your password before spending coins. This provides protection from wallet-stealing viruses and trojans as well as a sanity check before sending payments.")</p>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<div class="mirror-block">--}}
            {{--<div class="mirror-block__container container">--}}
              {{--<h2 class="mirror-block__title-mobile"><span>@lang("Vincoin Cash is private")</span></h2>--}}
              {{--<div class="mirror-block__col mirror-block__body">--}}
                {{--<h2 class="mirror-block__body__title"><span>@lang("Vincoin Cash is private")</span></h2>--}}
                {{--<p class="paragraph">@lang("Vincoin Cash uses ring signatures, ring confidential transactions and stealth addresses to obfuscate the origins, amounts and destinations of all transactions. Vincoin Cash provides all the benefits of a decentralized cryptocurrency, without any of the typical privacy concessions.")</p>--}}
              {{--</div>--}}
              {{--<div class="mirror-block__col mirror-block__img">--}}
                {{--<img class="lazy animated fadeIn" data-src="/images/difference/img-private.png" width="366" height="364" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt="">--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        </div>
      </div>

      <div class="section-wallet" id="section-wallet">
        <div class="section-wallet__container container">
          <div class="text-center mb-16 md:mb-12">
            <h2 class="section-wallet__title"><span>@lang("Download Vincoin Cash Wallet’s")</span></h2>
            <p class="section-wallet__desc">@lang("Vincoin Cash is securely stored in a wallet on your computer, tablet, phone or laptop. Download yours and start sending and receiving payments with an address and a click.")</p>
          </div>
          <div class="section-wallet__cards">
            @if($wallets['mac-os'])
                <div class="wallet__card" data-wallet-id="{{ $wallets['mac-os']->id }}">
                  <div class="info-text info-text--alt uppercase my-4">{{ $wallets['mac-os']->label }}</div>
                  <div class="wallet__card__img wallet__card__img-{{ $wallets['mac-os']->name }} animated fadeIn"></div>
                  <div class="wallet__card__img wallet__card__img-download animated fadeIn">
                    <a class="js-wallet-download" href="{{ asset($wallets['mac-os']->download_link) }}" data-wallet-id="{{ $wallets['mac-os']->id }}"></a>
                  </div>
                  <div class="wallet__card__download">
                    <a href="{{ asset($wallets['mac-os']->download_link) }}" class="link uppercase js-wallet-download">@lang("Download")</a>
                  </div>
                  <div class="wallet__card__version">
                    {{--<div class="badge badge--default">v{{ $wallets['mac-os']->version }} (Downloads: {{ $wallets['mac-os']->downloads_count }})</div>--}}
                    <div class="badge badge--default block">v{{ $wallets['mac-os']->version }}</div>
                    <div class="downloads-count">@lang("Downloads"): {{ $wallets['mac-os']->downloads_count }}</div>
                  </div>
                </div>
            @endif
              @if($wallets['win-pc'])
              <div class="wallet__card" data-wallet-id="{{ $wallets['win-pc']->id }}">
                <div class="info-text info-text--alt uppercase my-4">{{ $wallets['win-pc']->label }}</div>
                <div class="wallet__card__img wallet__card__img-{{ $wallets['win-pc']->name }} animated fadeIn"></div>
                <div class="wallet__card__img wallet__card__img-download animated fadeIn">
                  <a class="js-wallet-download" href="{{ asset($wallets['win-pc']->download_link) }}" data-wallet-id="{{ $wallets['win-pc']->id }}"></a>
                </div>
                <div class="wallet__card__download">
                  <a href="{{ asset($wallets['win-pc']->download_link) }}" class="link uppercase js-wallet-download">@lang("Download")</a>
                </div>
                <div class="wallet__card__version">
                  <div class="badge badge--default block">v{{ $wallets['win-pc']->version }}</div>
                  <div class="downloads-count">@lang("Downloads"): {{ $wallets['win-pc']->downloads_count }}</div>
                </div>
              </div>
              @endif
              @if($wallets['linux'])
              <div class="wallet__card" data-wallet-id="{{ $wallets['linux']->id }}">
                <div class="info-text info-text--alt uppercase my-4">{{ $wallets['linux']->label }}</div>
                <div class="wallet__card__img wallet__card__img-{{ $wallets['linux']->name }} animated fadeIn"></div>
                <div class="wallet__card__img wallet__card__img-download animated fadeIn">
                  <a class="js-wallet-download" href="{{ asset($wallets['linux']->download_link) }}" data-wallet-id="{{ $wallets['linux']->id }}"></a>
                </div>
                <div class="wallet__card__download">
                  <a href="{{ asset($wallets['linux']->download_link) }}" class="link uppercase js-wallet-download">@lang("Download")</a>
                </div>
                <div class="wallet__card__version">
                  <div class="badge badge--default block">v{{ $wallets['linux']->version }}</div>
                  <div class="downloads-count">@lang("Downloads"): {{ $wallets['linux']->downloads_count }}</div>
                </div>
              </div>
              @endif
              @if($wallets['github'])
              <div class="wallet__card" data-wallet-id="{{ $wallets['github']->id }}">
                <div class="info-text info-text--alt uppercase my-4">{{ $wallets['github']->label }}</div>
                <div class="wallet__card__img wallet__card__img-{{ $wallets['github']->name }} animated fadeIn"></div>
                <div class="wallet__card__img wallet__card__img-download animated fadeIn">
                  <a class="js-wallet-download" href="{{ asset($wallets['github']->download_link) }}" data-wallet-id="{{ $wallets['github']->id }}"></a>
                </div>
                <div class="wallet__card__download">
                  <a href="{{ asset($wallets['github']->download_link) }}" class="link uppercase js-wallet-download">@lang("Download")</a>
                </div>
                <div class="wallet__card__version">
                  <div class="badge badge--default block">v{{ $wallets['github']->version }}</div>
                  <div class="downloads-count">@lang("Downloads"): {{ $wallets['github']->downloads_count }}</div>
                </div>
              </div>
              @endif
          </div>
        </div>
      </div>

      <div class="section-choose" id="section-choose">
        <div class="section-choose__container container">
          <div class="section-choose__mine-col">
            <div class="section-choose__mine-col__title-wrap">
              <h2 class="section-choose__mine-col__title"><span>@lang("Mine Vincoin Cash")</span></h2>
            </div>
            <p class="section-choose__mine-col__desc">@lang("Mining of Vincoin Cash (VNC) can be achieved either by Solo mining, or pool mining. <br>Are you new to cryptocurrency and mining? Just <a href='#section-wallet' class='link link-anchor'>download the Vincoin Cash wallet</a>, set it up in seconds, and then click on the mining tab and hit ‘Start mining’ and receive Vincoin Cash (VNC) rewards.")</p>
            <div class="section-choose__mine-col__btn">
              <button class="btn btn--red" type="button">
                <a class="link-anchor" href="#section-wallet">@lang("Start mining")</a>
              </button>
            </div>
            <div class="section-choose__mine-col__img"><img class="lazy animated fadeIn" data-src="/images/choose/img-mine-vinecoin.png" width="516" height="441" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" alt=""></div>
          </div>
          <div class="section-choose__trade-col">
            <div class="section-choose__trade-col__title-wrap">
              <h2 class="section-choose__trade-col__title"><span>@lang("Trade Vincoin Cash")</span></h2>
              {{--TODO: Fix this on small screens later--}}
              <div class="badge badge--info flex-none ml-4 align-text-bottom my-2">@lang("Coming Soon")</div>
            </div>
            <p class="section-choose__trade-col__desc">@lang("Vincoin Cash can be easily purchased and sold on a number of exchanges using all types of currencies. Find the one that's right for you and get coins in minutes. *Vincoin Cash can be identified on exchanges under currency symbols VNC")</p>
            {{--<div class="mb-6">--}}
              {{--<button class="btn btn--red vin-table-toggle">Exchanges</button>--}}
            {{--</div>--}}
            <div class="section-choose__trade-col__table vin-table-holder">
              <table class="vin-table exchanges-table">
                <thead>
                <tr>
                  <td class="exchanges-table__name-col">@lang("Name")</td>
                  <td class="exchanges-table__pairs-col">@lang("Exchange pairs")</td>
                  <td class="exchanges-table__portal-col">@lang("Link to portal")</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td class="exchanges-table__name-col">@lang("GETBTC")</td>
                  <td class="exchanges-table__pairs-col-desktop">BTC/VNC</td>
                  <td class="exchanges-table__pairs-col-mobile"><a href="javascript:void(0);" class="link">BTC/VNC</a></td>
                  <td class="exchanges-table__portal-col"><a href="https://getbtc.org/en/trade?name=vnct_btc;" class="link">@lang("Go to service >")</a></td>
                </tr>
                <tr>
                  <td class="exchanges-table__name-col">@lang("Name")</td>
                  <td class="exchanges-table__pairs-col-desktop">BTC/BTX</td>
                  <td class="exchanges-table__pairs-col-mobile"><a href="javascript:void(0);" class="link">BTC/BTX</a></td>
                  <td class="exchanges-table__portal-col"><a href="javascript:void(0);" class="link">@lang("Go to service >")</a></td>
                </tr>
                <tr>
                  <td class="exchanges-table__name-col">@lang("Name")</td>
                  <td class="exchanges-table__pairs-col-desktop">BTC/BTX</td>
                  <td class="exchanges-table__pairs-col-mobile"><a href="javascript:void(0);" class="link">BTC/BTX</a></td>
                  <td class="exchanges-table__portal-col"><a href="javascript:void(0);" class="link">@lang("Go to service >")</a></td>
                </tr>
                </tbody>
              </table>
              {{--<div class="text-right mt-3"><a href="javascript:void(0);" id="vin-table-toggle" class="link vin-table-toggle">Close x</a></div>--}}
            </div>
          </div>
        </div>
      </div>

      <div class="section-spend bg-grey-lighter" id="section-spend">
        <div class="section-spend__container container text-center">
          <h2 class="heading heading--md text-underline mb-10"><span>@lang("Spend Vincoin")</span></h2>
          <p class="paragraph mb-12">@lang("Spend your Vincoin instantly and securely at these merchants and services")</p>
          <div class="cards spend-cards js-spend-slider js-mobile-slider">
            <div class="card"><a href="https://www.thaodienyoga.vn/"><img src="/images/spend/img-spend-1.png" alt=""></a></div>
            <div class="card"><a href="https://www.facebook.com/tornadoshishaking.delivery/"><img src="/images/spend/img-spend-2.png" alt=""></a></div>
            <div class="card"><a href="https://www.facebook.com/myspot.mytea/"><img src="/images/spend/img-spend-3.png" alt=""></a></div>
          </div>
        </div>
      </div>

      <div class="timeline-list-container"></div>

      <div class="section-roadmap" id="section-roadmap">
        {{--<div id="particles-js--roadmap"></div>--}}
        {{--<canvas id="cv-dot-waves"></canvas>--}}
        <div class="section-roadmap__container container">
          <div class="text-center mb-20 relative lg:mb-12">
            <h2 class="heading heading--md text-underline text-white"><span>@lang("Road Map")</span></h2>
          </div>

          <div class="timeline-container timeline-theme-1">
            <div class="timeline js-timeline">
              <div data-time="@lang("DEC 21, 2017")">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">@lang("DEC 21, 2017")</h3>
                  <div class="timeline__item__header">@lang("Vincoin Cash Launched")</div>
                  <div class="timeline__item__body">@lang("Congratulations! First Vincoin Cash nodes launched.")</div>
                </div>
              </div>
              <div data-time="@lang("JAN 2018")">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">@lang("JAN 2018")</h3>
                  <div class="timeline__item__header">@lang("Block Explorer")</div>
                  <div class="timeline__item__body">@lang("Launched block explorer for Vincoin Cash.")</div>
                </div>
              </div>
              <div data-time="@lang("JAN 2018")">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">@lang("JAN 2018")</h3>
                  <div class="timeline__item__header">@lang("Wallets")</div>
                  <div class="timeline__item__body">@lang("Windows, MacOS wallets created.")</div>
                </div>
              </div>
              <div data-time="@lang("FEB 2018")">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">@lang("FEB 2018")</h3>
                  <div class="timeline__item__header">@lang("Set up Mining")</div>
                  <div class="timeline__item__body">@lang("Set up mining tools and create user tutorials and documentation.")</div>
                </div>
              </div>
              <div data-time="@lang("MAR 2018")">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">@lang("MAR 2018")</h3>
                  <div class="timeline__item__header">@lang("Preparation")</div>
                  <div class="timeline__item__body">@lang("Preparatory work, testing")</div>
                </div>
              </div>
              <div data-time="@lang("MAY 2018")">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">@lang("MAY 2018")</h3>
                  <div class="timeline__item__header">@lang("Website released")</div>
                  <div class="timeline__item__body">@lang("Launched website and officially announced Vincoin Cash on social media.")</div>
                </div>
              </div>
              <div data-time="@lang("MAY 2018")">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">@lang("MAY 2018")</h3>
                  <div class="timeline__item__header">@lang("Community creation")</div>
                  <div class="timeline__item__body">@lang("Launched ambassadors program and started development of Vincoin Cash community.")</div>
                </div>
              </div>
              <div data-time="Q2 2018">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q2 2018</h3>
                  <div class="timeline__item__header">@lang("Vietnamese and Russian website language")</div>
                  <div class="timeline__item__body">@lang("Finished creating Vietnamese and Russian interface for the website.")</div>
                </div>
              </div>
              <div data-time="Q3 2018">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q3 2018</h3>
                  <div class="timeline__item__header"></div>
                  <div class="timeline__item__body">@lang("- Beginning of promotion campaign to inform and promote Vincoin Cash to the world.<br> - Airdrop and community program<br> - New release of the main website")</div>
                </div>
              </div>
              <div data-time="Q4 2018">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q4 2018</h3>
                  <div class="timeline__item__header"></div>
                  <div class="timeline__item__body">@lang("- New release of MacOS and Windows wallets<br> - Launch Ambassadors program<br> - Adding the Vietnamese language to the wallets<br> - Distribution of Airdrop coins to the community<br> - Listing Vincoin Cash on its first crypto exchange")</div>
                </div>
              </div>
              <div data-time="Q1 2019">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q1 2019</h3>
                  <div class="timeline__item__header"></div>
                  <div class="timeline__item__body">@lang("- Launch an active PR campaign<br> - Onboard influencers and reviewers")</div>
                </div>
              </div>
              <div data-time="Q2 2019">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q2 2019</h3>
                  <div class="timeline__item__header"></div>
                  <div class="timeline__item__body">@lang("- Start expanding in Cambodia and Laos<br> - Continue to build our local community<br> - Translate interface<br> - Organize meet-ups and conferences")</div>
                </div>
              </div>
              <div data-time="Q3 2019">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q3 2019</h3>
                  <div class="timeline__item__header"></div>
                  <div class="timeline__item__body">@lang("- Build partnerships with online and offline businesses<br> - Increase use cases of Vincoin Cash<br> - Update research regarding private blockchain currencies")</div>
                </div>
              </div>
              <div data-time="Q4 2019">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q4 2019</h3>
                  <div class="timeline__item__header"></div>
                  <div class="timeline__item__body">@lang("- Implementation of VNC to fiat exchange<br> - Launch educational platform about blockchain and crypto<br> - Launch Vincoin Cash labs<br> - Partner with educational press, portals, and courses")</div>
                </div>
              </div>
              <div data-time="Q1 2020">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q1 2020</h3>
                  <div class="timeline__item__header"></div>
                  <div class="timeline__item__body">@lang("- Last TestNET Vincoin Cash is mined. Launch of MAIN NET of VNC PROTOCOL <br> - Launch the Vincoin Cash online store ")</div>
                </div>
              </div>
              <div data-time="Q2 2020">
                <div class="box-item__inner">
                  <h3 class="timeline__item__quartal">Q2 2020</h3>
                  <div class="timeline__item__header"></div>
                  <div class="timeline__item__body">@lang("- Add VNC swipe functionality to any other blockchain 1=1")</div>
                </div>
              </div>
              {{--<div data-time="2021">--}}
                {{--<div class="box-item__inner">--}}
                  {{--<h3 class="timeline__item__quartal">2021</h3>--}}
                  {{--<div class="timeline__item__header">@lang("Started big Journey. Platform for business")</div>--}}
                  {{--<div class="timeline__item__body">@lang("We are expecting to get government accreditation as exchange coin in South-East Asia. Trying to get Accreditation from government.")</div>--}}
                {{--</div>--}}
              {{--</div>--}}
            </div>
          </div>
        </div>
      </div>

      <div class="section-about-us" id="section-about-us">
        <div class="section-about-us__container container">
          <div class="our-partners">
            <div class="text-center mb-24 md:mb-8">
              <h2 class="heading heading--md text-underline"><span>@lang("Press and Partners")</span></h2>
            </div>

            <div class="cards partners-cards js-mobile-slider">
              <div class="card"><a href="https://getbtc.org/"><img src="/images/partners/get-btc.png" alt=""></a></div>
              <div class="card"><a href="https://friendy.io"><img src="/images/partners/friendy.jpg" alt=""></a></div>
              <div class="card"><a href="https://www.asiablockchainreview.com/will-crypto-regulation-kill-the-initial-idea-of-bitcoin/"><img src="https://www.asiablockchainreview.com/wp-content/themes/abh/assets/images/logo-abh-header.svg" alt=""></a></div>
              {{--<div class="card"><a href="#"><img src="/images/partners/forbes.png" alt=""></a></div>--}}
            </div>
          </div>

          <div class="section-about-us__desc">
            <h2 class="heading heading--md text-underline mb-6"><span>@lang("About us")</span></h2>
            <p class="paragraph mb-10">@lang("We are a team of professionals with varying areas of expertise; a young, enterprising team that has been able to take advantage of previous projects. Our most valuable resource is our team of professionals. We strive to find the best talent who are passionate about our vision. We work hard to create the best environment for them to do things they love. We respect their previous achievements and also their privacy. We will introduce more about our team on our Medium in the future.")</p>
          </div>
          <h2 class="heading heading--md text-underline mb-8"><span>@lang("Team")</span></h2>
          <div class="our-team">
            <img class="lazy our-team__lazy-img js-delayed-section" data-src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA" width="0" height="0" src="" alt="">
            <div class="our-team__wrap">
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__img">
                    <img src="/images/team/img_nick.png" width="190" height="190" alt="">
                  </div>
                  <div class="team-member__desc">
                    <h3 class="team-member__name">Nick Vasilich</h3>
                    <div class="team-member__role">CMO</div>
                    <div class="team-member__social-acc">
                      <a href="https://www.linkedin.com/in/nick-vasilich-63677b154/" class="link" target="_blank">
                        <div class="team-member__social-acc__item">
                            <span class="icon icon-linkedIn"></span>
                            <span>LinkedIn</span>
                        </div>
                      </a>
                    </div>
                    <div class="divider divider--yellow"></div>
                    <div class="team-member__info">During the company’s early years,
                      teamplayer #1 acted as the sole
                      engineer, designer, and support.</div>
                  </div>
                </div>
              </div>
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__img">
                    <img src="/images/team/img_bugaevski.png" width="190" height="190" alt="">
                  </div>
                  <div class="team-member__desc">
                    <h3 class="team-member__name">Andrey Bugaevskyy</h3>
                    <div class="team-member__role">Research and Development</div>
                    <div class="team-member__social-acc">
                      <a href="https://www.linkedin.com/in/andriy-bugayevskyy/" class="link" target="_blank">
                        <div class="team-member__social-acc__item">
                          <span class="icon icon-linkedIn"></span>
                          <span>LinkedIn</span>
                        </div>
                      </a>
                    </div>
                    <div class="divider divider--yellow"></div>
                    <div class="team-member__info">During the company’s early years,
                      teamplayer #1 acted as the sole
                      engineer, designer, and support.</div>
                  </div>
                </div>
              </div>
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__img">
                    <img src="/images/team/img_olga.jpg" width="190" height="190" alt="">
                  </div>
                  <div class="team-member__desc">
                    <h3 class="team-member__name">Olga Surilova</h3>
                    <div class="team-meteam-member__imgmber__role">Project Manager</div>
                    <div class="team-member__social-acc">
                      <a href="http://linkedin.com/in/olga-surilova" class="link" target="_blank">
                        <div class="team-member__social-acc__item">
                          <span class="icon icon-linkedIn"></span>
                          <span>LinkedIn</span>
                        </div>
                      </a>
                    </div>
                    <div class="divider divider--yellow"></div>
                    <div class="team-member__info">During the company’s early years,
                      teamplayer #1 acted as the sole
                      engineer, designer, and support.</div>
                  </div>
                </div>
              </div>
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__img">
                    {{--<img src="" alt="">--}}
                    <img src="/images/team/img_makovetski.jpg" width="190" height="190" alt="">
                  </div>
                  <div class="team-member__desc">
                    <h3 class="team-member__name">Artem Makovetskiy</h3>
                    <div class="team-member__role">Design & Coding</div>
                    <div class="team-member__social-acc">
                      {{--<a href="#" class="link" target="_blank">--}}
                        {{--<div class="team-member__social-acc__item">--}}
                          {{--<span class="icon icon-linkedIn"></span>--}}
                          {{--<span>LinkedIn</span>--}}
                        {{--</div>--}}
                      {{--</a>--}}
                    </div>
                    <div class="divider divider--yellow"></div>
                    <div class="team-member__info">During the company’s early years,
                      teamplayer #1 acted as the sole
                      engineer, designer, and support.</div>
                  </div>
                </div>
              </div>
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__img">
                    {{--<img src="" alt="">--}}
                    <img src="/images/team/img_horvat.png" width="190" height="190" alt="">
                  </div>
                  <div class="team-member__desc">
                    <h3 class="team-member__name">Vladymyr Horvat</h3>
                    <div class="team-member__role">Design & Development</div>
                    <div class="team-member__social-acc">
                      <a href="https://www.linkedin.com/in/vgorvat/" class="link" target="_blank">
                        <div class="team-member__social-acc__item">
                          <span class="icon icon-linkedIn"></span>
                          <span>LinkedIn</span>
                        </div>
                      </a>
                    </div>
                    <div class="divider divider--yellow"></div>
                    <div class="team-member__info">During the company’s early years,
                      teamplayer #1 acted as the sole
                      engineer, designer, and support.</div>
                  </div>
                </div>
              </div>
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__img">
                    <img src="/images/team/img_michael.png" width="190" height="190" alt="">
                  </div>
                  <div class="team-member__desc">
                    <h3 class="team-member__name">Michael Reynolds</h3>
                    <div class="team-member__role">Marketing</div>
                    <div class="team-member__social-acc">
                      <a href="https://www.linkedin.com/in/michaeljohnreynolds/" class="link" target="_blank">
                        <div class="team-member__social-acc__item">
                          <span class="icon icon-linkedIn"></span>
                          <span>LinkedIn</span>
                        </div>
                      </a>
                    </div>
                    <div class="divider divider--yellow"></div>
                    <div class="team-member__info">During the company’s early years,
                      teamplayer #1 acted as the sole
                      engineer, designer, and support.</div>
                  </div>
                </div>
              </div>
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__img">
                    <img src="/images/team/img_vadym.png" width="190" height="190" alt="">
                  </div>
                  <div class="team-member__desc">
                    <h3 class="team-member__name">Vadym Chebanenko</h3>
                    <div class="team-member__role">Social media</div>
                    <div class="team-member__social-acc">
                      <a href="https://www.linkedin.com/in/vadym-chebanenko-148467160/" class="link" target="_blank">
                        <div class="team-member__social-acc__item">
                          <span class="icon icon-linkedIn"></span>
                          <span>LinkedIn</span>
                        </div>
                      </a>
                    </div>
                    <div class="divider divider--yellow"></div>
                    <div class="team-member__info">During the company’s early years,
                      teamplayer #1 acted as the sole
                      engineer, designer, and support.</div>
                  </div>
                </div>
              </div>
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__img">
                    <img src="/images/team/img_nikita.png" width="190" height="190" alt="">
                  </div>
                  <div class="team-member__desc">
                    <h3 class="team-member__name">Nikita Nekachaylo</h3>
                    <div class="team-member__role">Community manager</div>
                    <div class="team-member__social-acc">
                      <a href="https://www.linkedin.com/in/nick-nekachaylo-ab0958157/" class="link" target="_blank">
                        <div class="team-member__social-acc__item">
                          <span class="icon icon-linkedIn"></span>
                          <span>LinkedIn</span>
                        </div>
                      </a>
                    </div>
                    <div class="divider divider--yellow"></div>
                    <div class="team-member__info">During the company’s early years,
                      teamplayer #1 acted as the sole
                      engineer, designer, and support.</div>
                  </div>
                </div>
              </div>
              <div class="delayed delayed-hidden">
                <div class="team-member">
                  <div class="team-member__desc text-left">
                    <span class="font-bold">+ 6 more</span> people on backend and advisory support:<br>
                    • Risk Analyst<br>
                    • 2 Blockchain developers<br>
                    • Early investor and Advisor with trading background<br>
                    • SEA Market advisory board<br>
                    Strategy Advisor
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="mt-8 hidden">
            <button class="btn btn--red mx-auto">More</button>
          </div>
        </div>
      </div>

      <div class="section-coin-dist" id="section-coin-distribution">
        <div class="section-coin-dist__container container">
          <div class="coin-dist__inner">
            <div class="coin-dist__body">
              <h2 class="coin-dist__body__title"><span>@lang("Coin distribution")</span></h2>
              <ul class="coin-dist__body__list">
                <li class="coin-dist__body__list__item">15% @lang("Team (lock up up to 2 years)")</li>
                <li class="coin-dist__body__list__item">21% @lang("Seed and IEO (unlock every 6 months)")</li>
                <li class="coin-dist__body__list__item">15% @lang("Protocol Development(unlock every 6 months)")</li>
                <li class="coin-dist__body__list__item">49% @lang("Ecosystem/Operator")</li>
              </ul>
              <div class="info-text info-text--alt mb-2">@lang("Max possible total supply is <span class='no-wrap'>8 600 000 VNC</span>")</div>
              <div class="info-text info-text--alt">@lang("Bounty campaign included, <span class='no-wrap'>500 000 VNC</span>")</div>
            </div>
            <div class="coin-dist__chart-wrap">
              <div id="coin-dist__chart"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="section-other" id="section-other">
        <div class="section-other__container container">
          <div class="section-other__donate">
            <div class="section-other__title">
              <h2 class="heading heading--md text-underline mt-6 mb-20 lg:mt-0 lg:mb-8"><span>@lang("Donate to the ongoing development of Vincoin Cash")</span></h2>
            </div>
            <div class="QR-codes">
              <div class="QR-code">
                <div class="QR-code__label">BTC</div>
                <div>
                  <a href="/images/donate/QR-codes/BTC.svg" data-fancybox="images" data-caption="BTC">
                    <img class="QR-code__image" src="/images/donate/QR-codes/BTC.svg" alt="">
                  </a>
                </div>
                <div class="text-right mt-1">
                  <a class="link QR-code__zoom" href="/images/donate/QR-codes/BTC.svg" data-type="image" data-fancybox data-caption="My caption">@lang("Zoom")</a>
                  {{--<a class="link QR-code__copy-addr copy-btn" href="javascript:void(0);" data-clipboard-text="<copy text>">@lang("Copy address")</a>--}}
                </div>
              </div>
              <div class="QR-code">
                <div class="QR-code__label">ETH</div>
                <div>
                  <a href="/images/donate/QR-codes/ETH.svg" data-fancybox="images" data-caption="ETH">
                    <img class="QR-code__image" src="/images/donate/QR-codes/ETH.svg" alt="">
                  </a>
                </div>
                <div class="text-right mt-1"><a class="link QR-code__zoom" href="/images/donate/QR-codes/ETH.svg" data-type="image" data-fancybox data-caption="My caption">@lang("Zoom")</a></div>
              </div>
              <div class="QR-code">
                <div class="QR-code__label">LTC</div>
                <div>
                  <a href="/images/donate/QR-codes/LTC.svg" data-fancybox="images" data-caption="LTC">
                    <img class="QR-code__image" src="/images/donate/QR-codes/LTC.svg" alt="">
                  </a>
                </div>
                <div class="text-right mt-1"><a class="link QR-code__zoom" href="/images/donate/QR-codes/LTC.svg" data-type="image" data-fancybox data-caption="My caption">@lang("Zoom")</a></div>
              </div>
            </div>
          </div>
          <div class="section-other__faq" id="section-faq">
            <div class="section-other__title">
              <h2 class="heading heading--md text-underline mt-6 mb-20 lg:mt-0 lg:mb-8"><span>@lang("FAQ")</span></h2>
            </div>
            <ul class="accordion">
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("What principle defines the value of Vincoin Cash?") <span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("Vincoin Cash is a currency that changes its value based on the demand. For instance, if the demand exceeds the supply, Vincoin Cash will automatically gain value, thus increasing its selling price. Vice-versa also applies in this case. If the demand drops below the supply, Vincoin Cash will lose value and the selling price will drop.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("How can I obtain Vincoin Cash?") <span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("Vincoin Cash can be acquired in more than one way. You can eighter receive it through an exchange, or simply purchase it from an individual. Another way of gaining this currency is by mining Vincoin Cash from the Block Reward.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("What is a mnemonic seed?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("Think of the mnemonic seed as a password that helps you restore your account if something goes wrong (computer crash, faulty internet connection, etc). The mnemonic seed is a series of 25 random words than need to be put in the right order to restore access to the wallet. After creating the mnemonic seed, we recommend writing it down on a piece of paper rather than using computer-based solutions, for safety reasons.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("What are the privacy benefits of Vincoin Cash?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("Unlike other privacy systems for different coins, Vincoin Cash uses three different privacy technologies, reassuring the fact that the Vincoin Cash transactions are safe and most important, the transactions are anonimous. 1.Ring Signature – The ring is formed out of two pieces. The actual signer which is a one-time spend key that corressponds with an output being sent from the sender’s wallet and non-signers that resemble the past transaction outputs that are drawn from the Vincoin Cash BlockChain. The main benefit of the ring signature technology is the fact that it helps the sender to mask the origin of the transaction. 2. Ring CT – The purpose of the Ring CT is to hide the ammount of each transaction. Besides this, Ring CT will also make the payments unlinkable. 3.Stealth Addresses - Using a stealth address, the receiver of the income will generate a unique address in such a way that he will be the only one able to deduce the corresponding private key, thus resulting in an address that cannot be directly traced. Today, all Vincoin Cash transactions can only be performed using the privacy technologies mentioned above, therefore there is no risk of sending a transparent transaction.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("What are the differences between Vincoin Cash and Bitcoin?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("Unlike Bitcoin, Vincoin Cash offers more protection by using its CryptoNote protocol to hide information related to transactions. Vincoin Cash hides this information as its purpose is to protect user transaction privacy. Bitcoin system is transparent to all users and all made transactions are public to everyone. Bitcoin does not use stealth addresses and payments can be traced directly to the sender's address. Some of the features that Vincoin Cash comes with include dynamic block sizes, tail coin emission and ASIC-resistant environment. Bitcoin on the other hand offers advantages to people who are willing to invest into ASIC custom made CPUs for mining.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("Is Vincoin Cash block size limited?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("Vincoin Cash block size fluctuates over time as the currency changes its value according to demand. Vincoin Cash growth can be capped at certain intervals in oder to prevent ridiculous increases in amount.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("What does a Blockchain represent?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("A Blockchain represents a system that stores a copy of all transaction history on the Vincoin Cash network. Every two minutes, a new block with the latest transaction information is created and added to the Blockchain. This chain allows the network to verify the available balance in accounts and make it resilient to attacks and centralization attempts.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("What does fungibility represent and why is it important?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("Fungibility refers to a good or asset that is equivalent with another individual good or asset as long as they are identical in specification, therefore have the same value. An example would be currency, where two persons can exchange a 10 for two 5's and would still have the same value. However, supposedly that the 10's were previously used in a ransomware attack. This could make the trading seem shady for the other person and the trade could not take place anymore. Vincoin Cash is fungible, which means people do not need to go through this struggle.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("How can I be sure that Vincoin Cash is not created out of thin air?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("With Vincoin Cash, there is a unique key image asociated to every transaction output that can be generated only by the holder of that output and used once. Attempting to use a key image more than once will be rejected by the miners as \"double-spends\" and will not be added to a valid block. For every new transaction, miners verify that the key image has not already been used in a previous transaction, to ensure it's not a double-spend. The value of the inputs spent and the value of the outputs that are sent are encrypted and can only be seen by the recipient. However, the transaction amounts can be verified as valid through the Pedersem commitments, which are cryptographic algorythms. This means that even though the Vincoin Cash value of each inputs and outputs cannot be determined, the encrypted output amounts you create (includind the output for the recipient and a charge output back to the sender along with the unencrypted transaction fee) is equal to the value of the inputs that are being spent, therefore their sums can be verified as being equal and a legitimate transaction can be confirmed. No observers can reveal the amount of the inputs or outputs but they can use mathematical calculations on the Pedersen commitments to determine that no Vincoin Cash was created out of thin air.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("Why is my wallet taking so long to sync?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("Slow internet connections or faulty hard drives are the most common reasons for long lasting synchronizing processes. Nonetheless, the entire synchronizing process itself is quite time consuming due to the fact that: a. when running a full node locally, the computer requires a copy of the entire blockchain; b. when running a remote node, the computer still requires a copy of all outputs; Faster synchronizing times can be achieved by using a Lightweight Wallet, however, please have in mind that this type of wallet implies sacrificing some privacy for better sync performance.")
                </div>
              </li>
              <li class="accordion__item"><a href="javascript:void(0);" class="toggle">@lang("How does a Lightweight Wallet differ from a Normal Wallet?")<span class="accordion__item__arrow"></span></a>
                <div class="inner info-text">
                  @lang("A Lightweight Wallet enables sending view keys to nodes, scanning the blockchain for incoming transactions to your account and on your behalf. The information that the node receives is the time of the transaction, however, information related to either the ammount, the sender, or the recipient will not be exposed. A Normal Wallet on the other hand, can be used with a controlled node to avoid all privacy leaks, including the time of the transaction. Different types of wallet softwares allow users to control nodes therefore avoiding possible information leaks.")
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </main>
@endsection

@push('after_scripts')
  <script src="{{ asset("/js/pages/home.js") }}"></script>
@endpush
