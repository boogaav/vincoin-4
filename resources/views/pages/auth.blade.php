@extends("layouts.main")
@inject("localization_helper", "\App\Services\Localization\LocalizationHelper")

@section("content")
  <header class="main-header js-main-header--default">
    <div class="main-header__container">
      <!-- Hamburger icon -->
      <div class="main-header__container__nav-trigger js-nav-trigger"><span>@lang("navigation.main.Menu")</span></div>
      <div class="nav-desktop">
        <div class="logo-wrapper">
          <a href="{{ route('site.home') }}" class="logo">
            <span class="logo__brand-img"></span>
            <span class="logo__brand-name">Vincoin Cash</span>
          </a>
        </div>
        <div class="nav-sections">
          <div class="nav-secondary">
            <nav role="navigation" class="nav-wrapper">
              <ul class="nav">
                <li class="nav__item"><a class="nav__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
                <li class="nav__item"><a class="nav__link" href="{{ route("auth.showLogin") }}">@lang('navigation.main.Sign In/Sign Up')</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="language-wrapper">
        <div class="language-selector nav__item">
          <a class="nav__link" href="javascript:void(0);">
            <div class="flex items-center">
              {{ $localization_helper->getCurrLocale() }} <span class="nav__link__arrow"></span>
            </div>
          </a>
          <div class="nav__dropdown">
            <div class="nav__dropdown__cover"></div>
            <ul class="nav__dropdown__list js-select-language">
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl() }}" class="nav__link">EN</a></li>
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('ru') }}" class="nav__link">RU</a></li>
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('vn') }}" class="nav__link">VN</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="download-btn">
        <button class="btn" type="button"><a class="uppercase" href="{{ route("site.home") }}#section-wallet">@lang('navigation.main.Testnet is live')</a></button>
      </div>
    </div>
  </header>

  <div class="nav-mobile">
    <div class="nav-mobile__control nav-mobile__control--back btn-mobile--icon-back js-nav-trigger"><span>@lang("navigation.main.Back")</span></div>
    @guest
      <a href="{{ route('auth.showLogin') }}" class="nav-mobile__control nav-mobile__control--user btn-mobile--icon-user"><span>@lang("navigation.main.Sign In")</span></a>
    @endguest
    <div class="logo-wrapper flex-no-shrink">
      <div class="inline-block">
        <a href="{{ route('site.home') }}" class="logo logo--white logo--col">
          <span class="logo__brand-img"></span>
          <span class="logo__brand-name">Vincoin Cash</span>
        </a>
      </div>
    </div>
    <div class="flex-shrink overflow-scroll">
      <!-- Navigation -->
      <nav id="ml-menu" role="navigation" class="nav-wrapper ml-menu">
        <div class="menu__wrap">
          <ul data-menu="main" class="nav nav--col menu__level">
            <li class="nav__item menu__item"><a class="nav__link menu__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
            @guest
              <li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ route("auth.showLogin") }}">@lang('navigation.main.Sign In/Sign Up')</a></li>
            @endguest
          </ul>
        </div>
      </nav>
    </div>
    <div class="mt-2 mb-2 mx-auto"><button data-fancybox-close class="btn btn--transparent normal-case min-w-160 js-nav-trigger" type="button">@lang("navigation.main.Close Menu")</button></div>
  </div>

  <div class="section-auth">
    <div class="section-auth__container container">
      <div class="logo-big section-logo"></div>
      <div class="text-center mb-8">
        <h2 class="heading heading-md" id="auth-title">@lang("navigation.main.Sign In")</h2>
      </div>
      <div class="vin-form-wrapper auth-form-wrapper">
        <form action="{{ route('auth.check') }}" class="vin-form vin-form--transparent auth-form" method="POST">
          {{ csrf_field() }}
          <div class="form__holder">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="form-label">
                @lang("E-mail")
              </label>
              <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="your@mail.com" required autofocus>
              @if ($errors->has('email'))
                <div class="help-block" data-for="email"><strong>{{ $errors->first('email') }}</strong></div>
              @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="form-label">
                @lang("Password (at least 6 characters)")
              </label>
              <input type="password" id="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="@lang("Password")" required>
              @if ($errors->has('password'))
                <div class="help-block" data-for="password"><strong>{{ $errors->first('password') }}</strong></div>
              @endif
            </div>
            <div class="form-group" style="display: none;">
              <label for="password-confirm" class="form-label">
                {{ __("Confirm Password") }}
              </label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
            <div class="form-group checkbox-group">
              <label for="register" class="form-label checkbox-label">
                <input type="checkbox" id="register" name="register" class="form-checkbox" {{ old('register') ? 'checked' : '' }}>
                <span class="checkbox-mock-btn"></span>
                @lang("Register new user")
              </label>
              <div class="help-block" data-for="register"></div>
            </div>
            <div class="form-group text-center">
              <button class="btn btn--red submit-btn ladda-button mx-auto mt-8 mb-8" type="submit" id="auth-submit" data-style="zoom-in"><span class="ladda-label">@lang("navigation.main.Sign In")</span></button>
            </div>
            <div class="form-group text-center mt-4">
              <a href="{{ $localization_helper->getLocalizedUrl(null, 'password.request') }}" class="link">Forgot Your Password?</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
