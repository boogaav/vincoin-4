<?php

namespace App\Http\Middleware;

use App\Services\Localization\LocalizationHelper;
use Closure;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use function PHPSTORM_META\type;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = LocalizationHelper::getRouteLocale();

//        $request->session()->flush();
//        dd(session()->has("pref_locale"));
//        dd(LocalizationHelper::getClientLang($request->server('HTTP_ACCEPT_LANGUAGE')));
//        dd($request->server('HTTP_ACCEPT_LANGUAGE'));

//        if (! session()->has("pref_locale")) {
//            $locale = LocalizationHelper::getClientLang($request->server('HTTP_ACCEPT_LANGUAGE'));
//            session(['pref_locale' => $locale]);
//            if ($locale !== 'en') {
//                return redirect(LocalizationHelper::getLocalizedUrl($locale));
//            }
//            return redirect(LocalizationHelper::getLocalizedUrl());
//        }

        if (! is_null($locale)) {
            URL::defaults(['locale' => $locale]);
            App::setLocale($locale);
        }

        return $next($request);
    }
}
