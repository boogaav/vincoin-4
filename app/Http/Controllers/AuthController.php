<?php

namespace App\Http\Controllers;

use App\Services\Localization\LocalizationHelper;
use App\Services\RewardSystem\RewardHelper;
use App\UserProfile;
use Illuminate\Support\Facades\Auth;
use App\ReferralLink;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use RegistersUsers;
    use AuthenticatesUsers;

    public function showAuthForm()
    {
        return view('pages.auth');
    }
//    /**
//     * Where to redirect users after login.
//     *
//     * @var string
//     */
//    protected $redirectTo = "/account";

    protected function redirectTo()
    {
        return route("site.showAccount");
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function checkAuth(Request $request)
    {
//        dd($request->get('register'));
        if ($request->get('register') === 'on') {
            return $this->register($request);
        } else {
            return $this->login($request);
        }
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
//            'wallet_number' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
//    public function username()
//    {
//        return '';
//    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
//            'wallet_number' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
//            'telegram_nickname' => 'nullable|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
//            'wallet_number' => $data['wallet_number'],
//            'telegram_nickname' => $data['telegram_nickname'],
            'password' => bcrypt($data['password']),
//            'referrer_id' => '',
        ]);

        $profile = new UserProfile();
        $profile->user()->associate($user);
        $profile->save();

        // Check if user is referred
        if (!empty($ref_id_hash = Cookie::get("ref_id"))) {
            if ($ref_link = ReferralLink::where('referrer_id', $ref_id = ReferralLink::generateRefId($ref_id_hash, true))->first()) {
                $profile->referred_by = $ref_id[0];
                $profile->save();
                // TODO: check this if it's appropriate
//                $ref_link->expired = true;
                // Add bonus tokens to referrer
//                RewardHelper::addBonusTokensTo($ref_id, RewardHelper::BONUS_AMOUNT_A);
            }
        }

        // Create referral link for user
        $ref_url = ReferralLink::generateUrl($user->id);
        $ref_link = new ReferralLink(['url' => $ref_url]);
        $ref_link->referrer_id = $user->id;
        $ref_link->save();

        return $user;
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect(route('auth.showLogin'));
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }
}
