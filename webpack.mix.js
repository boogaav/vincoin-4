let mix = require('laravel-mix');
let tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .copy('resources/assets/js/pages', 'public/js/pages')
   .sass('resources/assets/sass/app.scss', 'public/css')
    .copy("resources/assets/fonts", "public/fonts")
    .copy("resources/assets/images", "public/images")
    .copy("resources/assets/vendor/conf", "public/vendor")
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('./tailwind.js') ]
    });

if (mix.inProduction()) {
    mix.version();
}

// mix.combine(['resources/assets/js/*'], 'public/js/combined.js');

// mix.browserSync('vincoin.local');
